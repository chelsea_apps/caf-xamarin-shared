﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Controls
{
    public class ScriptableWebView : WebView
    {
        public ScriptableWebView()
        {
            Navigating += OnNavigating;
        }

        public async Task<string> ExecuteJavaScriptWithResultAsync(string script)
        {
            // Cope with race condition where page could attempt to execute JS before WebView has been created
            var func = await executeJavaScriptFuncTcs.Task;
            return await func(script);
        }

        TaskCompletionSource<Func<string, Task<string>>> executeJavaScriptFuncTcs = new TaskCompletionSource<Func<string, Task<string>>>();

        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public void SetExecuteJavaScriptFunc(Func<string, Task<string>> func)
        {
            executeJavaScriptFuncTcs.TrySetResult(func);
        }

        public Func<Uri, bool> ShouldStartLoad { get; set; }

        public static readonly BindableProperty OpenLinksInExternalBrowserProperty = BindableProperty.Create(nameof(OpenLinksInExternalBrowser), typeof(bool), typeof(ScriptableWebView), false);

        public bool OpenLinksInExternalBrowser
        {
            get { return (bool)GetValue(OpenLinksInExternalBrowserProperty); }
            set { SetValue(OpenLinksInExternalBrowserProperty, value); }
        }

        string initialUrl;

        public static BindableProperty UserAgentProperty = BindableProperty.Create(nameof(UserAgent), typeof(string), typeof(ScriptableWebView), string.Empty);

        public string UserAgent
        {
            get { return (string)GetValue(UserAgentProperty); }
            set { SetValue(UserAgentProperty, value); }
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);
            if(propertyName == SourceProperty.PropertyName && initialUrl == null)
            {
                var urlSource = Source as UrlWebViewSource;
                if(urlSource != null)
                {
                    initialUrl = urlSource.Url;
                }
            }
        }

        protected void OnNavigating(object sender, WebNavigatingEventArgs args)
        {
            if(!OpenLinksInExternalBrowser)
            {
                return;
            }
            if(args.NavigationEvent == WebNavigationEvent.NewPage)
            {
                if(args.Url != null && args.Url != initialUrl && args.Url.StartsWith("http", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        var uri = new Uri(args.Url);
                        Device.OpenUri(uri);
                    }
                    catch(Exception)
                    {
                    }
                    finally
                    {
                        args.Cancel = true;
                        RaiseNavigationCanceled(args);
                    }
                }
            }
        }

        public event EventHandler<WebNavigationCanceledEventArgs> NavigationCanceled;

        void RaiseNavigationCanceled(WebNavigationEventArgs args)
        {
            NavigationCanceled?.Invoke(this, new WebNavigationCanceledEventArgs(args.NavigationEvent, args.Source, args.Url));
        }

        /// <summary>
        /// Waits for an element to exist
        /// </summary>
        public async Task WaitForElementAsync(string elementId)
        {
            while(true)
            {
                var elementExists = await ExecuteJavaScriptWithResultAsync($"document.getElementById('{elementId}') !== null");
                if(elementExists == "true")
                {
                    return;
                }

                await Task.Delay(10);
            }
        }
    }

    public class WebNavigationCanceledEventArgs : WebNavigationEventArgs
    {
        public WebNavigationCanceledEventArgs(WebNavigationEvent e, WebViewSource source, string url) : base(e, source, url)
        {
            // I would have prefered to just use WebNavigationEventArgs, but it has no public constructor
        }
    }
}

