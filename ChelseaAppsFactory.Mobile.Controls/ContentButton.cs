﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Controls
{
    [ContentProperty("Content")]
    public class ContentButton : Grid
    {
        readonly Button button;

        public ContentButton()
        {
            button = new FlatButton() { BorderWidth = 0, BackgroundColor = Color.Transparent, IsEnabled = IsEnabled };
            button.Clicked += (sender, e) => Clicked?.Invoke(this, e);
            Children.Add(button);

            RowDefinitions = new RowDefinitionCollection { new RowDefinition() };
            UpdateHeight();
        }

        public event EventHandler Clicked;

        public static readonly BindableProperty ContentProperty = BindableProperty.Create(
            propertyName: nameof(Content),
            returnType: typeof(View),
            declaringType: typeof(ContentButton),
            defaultValue: default(View),
            propertyChanged: ContentChanged);

        static void ContentChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var item = bindable as ContentButton;
            if(oldValue != null)
            {
                item.Children.Remove((View)oldValue);
            }

            var newView = (View)newValue;
            newView.InputTransparent = true;
            item.Children.Add(newView);
        }

        public View Content
        {
            get { return (View)GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        public static readonly BindableProperty CommandProperty = BindableProperty.Create(
            propertyName: nameof(Command),
            returnType: typeof(ICommand),
            declaringType: typeof(ContentButton),
            defaultValue: default(Command),
            propertyChanged: CommandPropertyChanged);

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        static void CommandPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var item = bindable as ContentButton;
            var command = newValue as ICommand;
            item.button.Command = command;
        }

        public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create(
            propertyName: nameof(CommandParameter),
            returnType: typeof(object),
            declaringType: typeof(ContentButton),
            defaultValue: default(object),
            propertyChanged: CommandParameterPropertyChanged);

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        static void CommandParameterPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var item = bindable as ContentButton;
            item.button.CommandParameter = newValue;
        }

        public new string AutomationId
        {
            get { return button.AutomationId; }
            set { button.AutomationId = value; }
        }

        protected override void OnPropertyChanged(string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if(propertyName == nameof(HeightRequest))
            {
                UpdateHeight();
            }

            if(propertyName == nameof(IsEnabled))
            {
                button.IsEnabled = IsEnabled;
            }
        }

        void UpdateHeight()
        {
            if(HeightRequest > 0 && RowDefinitions.Count == 1)
            {
                RowDefinitions[0].Height = HeightRequest;
            }
        }
    }
}