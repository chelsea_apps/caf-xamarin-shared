﻿using System;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Controls
{
    public class ExtendedFrame : Frame
    {
        public static readonly BindableProperty BorderRadiusProperty = BindableProperty.Create(nameof(BorderRadius), typeof(double), typeof(ExtendedFrame), 0.0);

        public double BorderRadius
        { 
            get { return (double)GetValue(BorderRadiusProperty); } 
            set { SetValue(BorderRadiusProperty, value); }
        }
    }
}

