using System;
using System.Linq;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Controls
{
    public class GradientView : View
    {
        public static readonly BindableProperty ColorsProperty = BindableProperty.Create(nameof(Colors), typeof(IEnumerable<Color>), typeof(GradientView), null);

        public IEnumerable<Color> Colors
        {
            get { return (IEnumerable<Color>)GetValue(ColorsProperty); }
            set { SetValue(ColorsProperty, value); }
        }

        public static readonly BindableProperty OrientationProperty = BindableProperty.Create(nameof(Orientation), typeof(GradientOrientation), typeof(GradientView), GradientOrientation.Horizontal);

        public GradientOrientation Orientation
        {
            get { return (GradientOrientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }
    }

    public enum GradientOrientation
    {
        Horizontal,
        Vertical
    }
}