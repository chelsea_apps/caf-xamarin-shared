﻿using System;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Controls
{
    public class ExtendedEditor : Editor
    {
        public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create(nameof(Placeholder), typeof(string), typeof(ExtendedEditor), string.Empty);

        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        public static readonly BindableProperty PlaceholderFontFamilyProperty = BindableProperty.Create(nameof(PlaceholderFontFamily), typeof(string), typeof(ExtendedEditor), string.Empty);

        public string PlaceholderFontFamily
        {
            get { return (string)GetValue(PlaceholderFontFamilyProperty); }
            set { SetValue(PlaceholderFontFamilyProperty, value); }
        }

        public static readonly BindableProperty PlaceholderFontSizeProperty = BindableProperty.Create(nameof(PlaceholderFontSize), typeof(float), typeof(ExtendedEditor), 15f);

        public float PlaceholderFontSize
        {
            get { return (float)GetValue(PlaceholderFontSizeProperty); }
            set { SetValue(PlaceholderFontSizeProperty, value); }
        }

        public static readonly BindableProperty PlaceholderTextColourProperty = BindableProperty.Create(nameof(PlaceholderTextColour), typeof(Color), typeof(ExtendedEditor), Color.Black);

        public Color PlaceholderTextColour
        {
            get { return (Color)GetValue(PlaceholderTextColourProperty); }
            set { SetValue(PlaceholderTextColourProperty, value); }
        }

        public static readonly BindableProperty MaxCharactersProperty = BindableProperty.Create(nameof(MaxCharacters), typeof(int), typeof(ExtendedEditor), 300);

        public int MaxCharacters
        {
            get { return (int)GetValue(MaxCharactersProperty); }
            set { SetValue(MaxCharactersProperty, value); }
        }

        public static readonly BindableProperty HasDefaultFocusProperty = BindableProperty.Create(
            propertyName: nameof(HasDefaultFocus),
            returnType: typeof(bool),
            declaringType: typeof(ExtendedEditor),
            defaultValue: false);

        public bool HasDefaultFocus
        {
            get { return (bool)GetValue(HasDefaultFocusProperty); }
            set { SetValue(HasDefaultFocusProperty, value); }
        }
    }
}
