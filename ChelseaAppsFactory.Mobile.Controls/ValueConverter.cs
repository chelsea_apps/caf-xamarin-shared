using System;
using System.Globalization;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Controls
{
    /// <summary>
    /// Generic ValueConverter helper class
    /// </summary>
    public abstract class ValueConverter<TFrom, TTo> : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = default(TFrom);

            if (value != null)
                val = (TFrom)value;
            
            return Convert(val, targetType, parameter, culture);
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = default(TTo);

            if (value != null)
                val = (TTo)value;
            
            return ConvertBack(val, targetType, parameter, culture);
        }

        public abstract TTo Convert(TFrom value, Type targetType, object parameter, CultureInfo culture);
        public abstract TFrom ConvertBack(TTo value, Type targetType, object parameter, CultureInfo culture);
    }   
}