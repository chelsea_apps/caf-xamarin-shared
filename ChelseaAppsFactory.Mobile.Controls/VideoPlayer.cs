﻿using System;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Controls
{
    public class VideoPlayer : View
    {
        public string FileName
        {
            get { return (string)GetValue(FileNameProperty); }
            set { SetValue(FileNameProperty, value); }
        }

        public static readonly BindableProperty FileNameProperty = BindableProperty.Create(nameof(FileName), typeof(string), typeof(VideoPlayer), string.Empty);
    }
}
