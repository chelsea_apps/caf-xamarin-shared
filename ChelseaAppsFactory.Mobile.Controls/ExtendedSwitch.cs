﻿using System;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Controls
{
    public class ExtendedSwitch : Switch
    {
        public static readonly BindableProperty TintColorProperty = BindableProperty.Create(nameof(TintColor), typeof(Color), typeof(ExtendedSwitch), Color.Default);

        public Color TintColor
        {
            get { return (Color)GetValue(TintColorProperty); }
            set { SetValue(TintColorProperty, value); }
        }
    }
}

