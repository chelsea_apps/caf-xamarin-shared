﻿using System;
using Xamarin.Forms;
using System.Windows.Input;

namespace ChelseaAppsFactory.Mobile.Controls
{
    public class ImageButton<T> : Grid where T : Image, new()
    {
        T image { get; set; }
        Button button { get; set; }
        Label textLabel { get; set; }

        public ImageButton()
        {
            // define rows to accommodate label
            RowSpacing = 0;
            RowDefinitions.Add(new RowDefinition { Height = GridLength.Star });
            RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

            image = new T();
            Children.Add(image);

            image.HorizontalOptions = LayoutOptions.Center;


            textLabel = new Label
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalOptions = LayoutOptions.StartAndExpand,
                VerticalTextAlignment = TextAlignment.Start,
                IsVisible = false,
            };
            SetRow(textLabel, 1);
            Children.Add(textLabel);

            button = new FlatButton() { BorderWidth = 0, BackgroundColor = Color.Transparent };
            SetRowSpan(button, 2);
            Children.Add(button);
        }

        internal T InternalImage => image;

        public static readonly BindableProperty ImageProperty = BindableProperty.Create(
            propertyName: nameof(Image),
            returnType: typeof(ImageSource),
            declaringType: typeof(ImageButton<T>),
            defaultValue: default(ImageSource),
            propertyChanged: ImagePropertyChanged);

        public ImageSource Image
        {
            get { return (ImageSource)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }

        static void ImagePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var imageButton = bindable as ImageButton<T>;
            var source = newValue as ImageSource;
            imageButton.image.Source = source;
        }

        public static readonly BindableProperty CommandProperty = BindableProperty.Create(
            propertyName: nameof(Command),
            returnType: typeof(ICommand),
            declaringType: typeof(ImageButton<T>),
            defaultValue: default(Command),
            propertyChanged: CommandPropertyChanged);

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        static void CommandPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var imageButton = bindable as ImageButton<T>;
            var command = newValue as ICommand;
            imageButton.button.Command = new Command(obj =>
            {
                if(imageButton.IsEnabled)
                {
                    command.Execute(obj);
                }
            });
        }

        public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create(
            propertyName: nameof(CommandParameter),
            returnType: typeof(object),
            declaringType: typeof(ImageButton<T>),
            defaultValue: default(object),
            propertyChanged: CommandParameterPropertyChanged);

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        static void CommandParameterPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var button = bindable as ImageButton<T>;
            button.button.CommandParameter = newValue;
        }

        public static readonly BindableProperty AspectProperty = BindableProperty.Create(nameof(Aspect), typeof(Aspect), typeof(ImageButton<T>), default(Aspect), propertyChanged: AspectPropertyChanged);

        public Aspect Aspect
        {
            get { return (Aspect)GetValue(AspectProperty); }
            set { SetValue(AspectProperty, value); }
        }

        static void AspectPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var button = bindable as ImageButton<T>;
            button.image.Aspect = (Aspect)newValue;
        }

        public static readonly BindableProperty ButtonWidthRequestProperty = BindableProperty.Create(nameof(ButtonWidthRequest), typeof(double), typeof(ImageButton<T>), 32.0, propertyChanged: ButtonWidthRequestPropertyChanged);

        public double ButtonWidthRequest
        {
            get { return (double)GetValue(ButtonWidthRequestProperty); }
            set { SetValue(ButtonWidthRequestProperty, value); }
        }

        static void ButtonWidthRequestPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var button = bindable as ImageButton<T>;
            button.button.WidthRequest = (double)newValue;
        }

        public static readonly BindableProperty ButtonHeightRequestProperty = BindableProperty.Create(nameof(ButtonHeightRequest), typeof(double), typeof(ImageButton<T>), 32.0, propertyChanged: ButtonHeightRequestPropertyChanged);

        public double ButtonHeightRequest
        {
            get { return (double)GetValue(ButtonHeightRequestProperty); }
            set { SetValue(ButtonHeightRequestProperty, value); }
        }

        static void ButtonHeightRequestPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var button = bindable as ImageButton<T>;
            button.button.HeightRequest = (double)newValue;
        }

        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(ImageButton<T>), propertyChanged: TextPropertyChanged);
        public static readonly BindableProperty TextColorProperty = BindableProperty.Create(nameof(TextColor), typeof(Color), typeof(ImageButton<T>), Color.Default, propertyChanged: TextColorPropertyChanged);
        public static readonly BindableProperty TextFontNameProperty = BindableProperty.Create(nameof(TextFontName), typeof(string), typeof(ImageButton<T>), propertyChanged: TextFontNamePropertyChanged);
        public static readonly BindableProperty TextFontSizeProperty = BindableProperty.Create(nameof(TextFontSize), typeof(double), typeof(ImageButton<T>), -1d, propertyChanged: TextFontSizePropertyChanged);

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        public string TextFontName
        {
            get { return (string)GetValue(TextFontNameProperty); }
            set { SetValue(TextFontNameProperty, value); }
        }

        public double TextFontSize
        {
            get { return (double)GetValue(TextFontSizeProperty); }
            set { SetValue(TextFontSizeProperty, value); }
        }

        static void TextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var ib = bindable as ImageButton<T>;
            ib.textLabel.Text = (string)newValue;
            ib.textLabel.IsVisible = !string.IsNullOrEmpty((string)newValue);
        }

        static void TextColorPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var ib = bindable as ImageButton<T>;
            ib.textLabel.TextColor = (Color)newValue;
        }

        static void TextFontNamePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var ib = bindable as ImageButton<T>;
            ib.textLabel.FontFamily = (string)newValue;
        }

        static void TextFontSizePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var ib = bindable as ImageButton<T>;
            ib.textLabel.FontSize = (double)newValue;
        }
    }

    public class ImageButton : ImageButton<Image>
    {
    }
}


