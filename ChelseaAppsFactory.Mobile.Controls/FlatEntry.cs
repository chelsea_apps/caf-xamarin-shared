﻿using System;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Controls
{
    public class FlatEntry : Entry
    {
        public static readonly BindableProperty MaximumLengthProperty = BindableProperty.Create(nameof(MaximumLength), typeof(int), typeof(FlatEntry), -1);

        public int MaximumLength 
        { 
            get { return (int)GetValue(MaximumLengthProperty); }
            set { SetValue(MaximumLengthProperty, value); }
        }

        public static readonly BindableProperty FormatterProperty = BindableProperty.Create(nameof(Formatter), typeof(ValueConverter<string, string>), typeof(FlatEntry), defaultValue: null);

        public ValueConverter<string, string> Formatter
        {
            get { return (ValueConverter<string, string>)GetValue(FormatterProperty); }
            set { SetValue(FormatterProperty, value); }
        }
    }
}

