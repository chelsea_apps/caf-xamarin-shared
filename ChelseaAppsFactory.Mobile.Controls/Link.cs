using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Controls
{
    public class Link : Label
    {
        public Link()
        {
            GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(_ => Clicked())
            });
        }

        void Clicked()
        {
            Command?.Execute(null);
        }

        public static readonly BindableProperty CommandProperty = BindableProperty.Create(
        	propertyName: nameof(Command),
        	returnType: typeof(ICommand),
        	declaringType: typeof(Link),
            defaultValue: default(Command));

        public ICommand Command
        {
        	get { return (ICommand)GetValue(CommandProperty); }
        	set { SetValue(CommandProperty, value); }
        }
   }
}