﻿using System;
using ChelseaAppsFactory.Mobile.Common;
using ChelseaAppsFactory.Mobile.Routing;
using ChelseaAppsFactory.Mobile.TestApp.Pages;
using ChelseaAppsFactory.Mobile.TestApp.ViewModels;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace ChelseaAppsFactory.Mobile.TestApp
{
    public partial class App : Application
    {
        public static new App Current => (App)Application.Current;

        public Router Router { get; private set; }
        readonly NavigationPage _root = new NavigationPage();

        public App()
        {
            Logger.Warn("Hello");

            Logger.Warn("Hi there, this is an extremely long message " +
                        "that will try to wrap! At some point. During this text. " +
                        "Hi there, this is an extremely long message " +
                        "that will try to wrap! At some point. During this text. " +
                        "Hi there, this is an extremely long message " +
                        "that will try to wrap! At some point. During this text. " +
                        "Hi there, this is an extremely long message " +
                        "that will try to wrap! At some point. During this text. " +
                        "Hi there, this is an extremely long message " +
                        "that will try to wrap! At some point. During this text. " +
                        "Hi there, this is an extremely long message " +
                        "that will try to wrap! At some point. During this text. " +
                        "Hi there, this is an extremely long message " +
                        "that will try to wrap! At some point. During this text. " +
                        "Hi there, this is an extremely long message " +
                        "that will try to wrap! At some point. During this text. " +
                        "Hi there, this is an extremely long message " +
                        "that will try to wrap! At some point. During this text. " +
                        "Hi there, this is an extremely long message " +
                        "that will try to wrap! At some point. During this text. " +
                        "Hi there, this is an extremely long message " +
                        "that will try to wrap! At some point. During this text. " +
                        "Hi there, this is an extremely long message " +
                        "that will try to wrap! At some point. During this text. " +
                        "Hi there, this is an extremely long message " +
                        "that will try to wrap! At some point. During this text. " +
                        "Hi there, this is an extremely long message " +
                        "that will try to wrap! At some point. During this text. ");

            Logger.Warn(string.Concat(Enumerable.Repeat("wrap me", 10000)));

            InitializeComponent();
            RegisterViewModels();

            Router = new Router(BuildRoutes(), _root, GetViewModel, GetMultiPage);

            var startPage = new StartPage { BindingContext = new StartViewModel() };
            _root.PushAsync(startPage, false);

            MainPage = _root;
        }

        static RouteConfig BuildRoutes()
        {
            var builder = new RouteConfigBuilder();

            builder.MapSinglePage<StartViewModel, StartPage>();
            builder.MapModal<ModalViewModel1, ModalPage1>();
            builder.MapSinglePage<ModalViewModel2, ModalPage2>();

            return builder.Build();
        }

        void RegisterViewModels()
        {
            AddViewModel<ModalViewModel1>(() => new ModalViewModel1());
            AddViewModel<ModalViewModel2>(() => new ModalViewModel2());
        }

        readonly Dictionary<Type, Func<IPageViewModel>> _viewModelsInits = new Dictionary<Type, Func<IPageViewModel>>();
        void AddViewModel<T>(Func<IPageViewModel> init) => _viewModelsInits.Add(typeof(T), init);
        IPageViewModel GetViewModel(Type t) => _viewModelsInits[t]();

        MultiPage<Page> GetMultiPage() => new TabbedPage();
    }
}