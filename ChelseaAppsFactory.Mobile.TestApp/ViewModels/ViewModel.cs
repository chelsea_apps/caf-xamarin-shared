﻿using System.Threading.Tasks;
using ChelseaAppsFactory.Mobile.Common;
using System.Windows.Input;

namespace ChelseaAppsFactory.Mobile.TestApp.ViewModels
{
	public abstract class ViewModel : IPageViewModel
	{
		public void CleanUp() { }
		public Task InitAsync() => Task.CompletedTask;
		public Task OnAppearingAsync() => Task.CompletedTask;
		public Task OnDisappearingAsync() => Task.CompletedTask;

		readonly IRouter _router = App.Current.Router;

		public ICommand BackCommand => new DelegateCommand(_ =>
		{
			_router.PopAsync().LogIfFaulted();
		});
	}

	public class GotoCommand<TViewModel> : DelegateCommand where TViewModel : IPageViewModel
	{
		public GotoCommand() : base(_ => App.Current.Router.Goto(OpenViewMessage.For<TViewModel>()))
		{         
		}
	}
}