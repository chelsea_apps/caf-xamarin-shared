﻿using System;
using System.Windows.Input;

namespace ChelseaAppsFactory.Mobile.TestApp.ViewModels
{
	public class ModalViewModel1 : ViewModel
    {
		public ICommand NextCommand => new GotoCommand<ModalViewModel2>();
    }
}
