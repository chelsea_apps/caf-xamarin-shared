﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ChelseaAppsFactory.Mobile.Common
{
    /// <summary>
    /// A list with a title. Useful for ListView grouping.
    /// </summary>
    public class TitledList<T> : List<T>, IGrouping<string, T>
    {
        public TitledList()
        {
        }

        public TitledList(IEnumerable<T> collection)
        {
            AddRange(collection);
        }

        public string Title { get; set; }

        public string Key => Title;
    }
}
