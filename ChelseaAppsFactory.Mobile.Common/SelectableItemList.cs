﻿﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ChelseaAppsFactory.Mobile.Common
{
    /// <summary>
    /// A readonly collection of items wrapped in a SelectableItem
    /// </summary>
    public class SelectableItemList<T> : SelectableItemList<T, SelectableItemList<T>.SelectableItem> where T : class
	{
		public SelectableItemList(IEnumerable<T> list) : base(list)
		{
		}

		public class SelectableItem : NotifyPropertyChangedBase, ISelectableItem
		{
			T _item;
			public T Item
			{
				get { return _item; }
				set { SetProperty(ref _item, value); }
			}

			bool _isSelected;
			public bool IsSelected
			{
				get { return _isSelected; }
				set { SetProperty(ref _isSelected, value); }
			}
		}

		public interface ISelectableItem : INotifyPropertyChanged
		{
			bool IsSelected { get; set; }
			T Item { get; set; }
		}        
    }

	/// <summary>
	/// A readonly collection of items wrapped in a custom SelectableItem
	/// </summary>
	public class SelectableItemList<TItem, TSelectable> : ReadOnlyCollection<TSelectable>, INotifyPropertyChanged
		where TItem : class
		where TSelectable : class, SelectableItemList<TItem>.ISelectableItem, new()
    {
        public SelectableItemList(IEnumerable<TItem> list) : base(list.Select(x => new TSelectable { Item = x}).ToList())
        {
            SelectedItem = Items.FirstOrDefault();
            Subscribe();
            if (SelectedItem != null)
            {
                SelectedItem.IsSelected = true;
            }
        }

        void Subscribe()
        {
			foreach (var item in Items)
			{
				item.PropertyChanged += Item_PropertyChanged;
			}
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public bool AllowMultipleSelection { get; set; }

        TSelectable _selectedItem;
        public TSelectable SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (_selectedItem != value)
                {
                    _selectedItem = value;
                    if (_selectedItem == null)
                    {
                        DeselectAll();
                    }
                    else
                    {
                        _selectedItem.IsSelected = true;
                    }
                    OnPropertyChanged(nameof(SelectedItem));
                }
            }
        }

        public List<TSelectable> SelectedItems => Items.Where(x => x.IsSelected).ToList();

        public void SelectItems(IEnumerable<TSelectable> toSelect)
        {
            foreach (var item in toSelect)
            {
                item.IsSelected = true;
            }
        }

        public void SelectAll()
        {
            SelectAll(null);
        }

        public void DeselectAll()
        {
            DeselectAll(null);
        }

		void Item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectableItemList<TItem>.ISelectableItem.IsSelected) && ((TSelectable)sender).IsSelected)
            {
                var initialSelectedItem = SelectedItem;
                SelectedItem = (TSelectable)sender;

                if (!AllowMultipleSelection)
                {
                    DeselectAll(SelectedItem);
                }
            }
        }

        void SelectAll(TSelectable selectedItem)
        {
			foreach (var item in Items)
			{
				if (item != selectedItem)
				{
					item.IsSelected = true;
				}
			}
        }

        void DeselectAll(TSelectable selectedItem)
        {
			foreach (var item in Items)
			{
				if (item != selectedItem)
				{
					item.IsSelected = false;
				}
			}
        }
    }
}
