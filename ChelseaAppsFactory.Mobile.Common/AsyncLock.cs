﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ChelseaAppsFactory.Mobile.Common
{
	public class AsyncLock
	{
		public struct Releaser : IDisposable
		{
			readonly AsyncLock _toRelease;

			internal Releaser(AsyncLock l)
			{
				_toRelease = l;
			}

			public void Dispose()
			{
				if (_toRelease != null)
				{
#if DEBUG
					if (_toRelease._lockName != null)
						Logger.Debug($"AsyncLock: Releasing lock on {_toRelease._lockName}");
#endif
					_toRelease._semaphore.Release();
#if DEBUG
					if (_toRelease._lockName != null)
						Logger.Debug($"AsyncLock: Released lock on {_toRelease._lockName}");
#endif
				}
			}
		}
		readonly SemaphoreSlim _semaphore;
		readonly Task<Releaser> _releaser;
        readonly string _lockName;

		public AsyncLock(string lockName = null)
		{
			_semaphore = new SemaphoreSlim(1);
			_releaser = Task.FromResult(new Releaser(this));
			_lockName = lockName;
		}

		public Task<Releaser> LockAsync()
		{
			var wait = _semaphore.WaitAsync();
			return wait.IsCompleted ? _releaser : wait.ContinueWith((_, state) => new Releaser((AsyncLock)state), this, CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);
		}

		public bool IsLocked => _semaphore.CurrentCount == 0;
	}
}