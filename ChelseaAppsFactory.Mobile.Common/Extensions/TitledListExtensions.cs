﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ChelseaAppsFactory.Mobile.Common.Extensions
{
    public static class TitledListExtensions
    {
        public static List<TitledList<T>> ToSingleGroupList<T>(this IEnumerable<T> collection, string title)
        {
			var list = new List<TitledList<T>>();
			var group = new TitledList<T>(collection) { Title = title };
            list.Add(group);
            return list;
        }

        public static List<TitledList<T>> ToGroupList<T>(this IEnumerable<IGrouping<string, T>> collection)
        {
			var list = new List<TitledList<T>>();
			foreach (var group in collection)
            {
                list.Add(new TitledList<T>(group) { Title = group.Key });
            }
            return list;
        }
    }
}
