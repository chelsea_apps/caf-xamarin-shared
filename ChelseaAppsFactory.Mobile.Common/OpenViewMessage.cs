﻿using System;
using System.Linq;
using System.Reflection;

namespace ChelseaAppsFactory.Mobile.Common
{
	public class OpenViewMessage
	{
		public static OpenViewMessage For<T>(Action<T> initializer = null) where T : IPageViewModel
		{
			return new OpenViewMessage
			{
				ViewInitializer = ConvertInitializer(initializer),
				ViewType = typeof(T)
			};
		}

		public static OpenViewMessage ForQueue<T>(Action<T> initializer = null) where T : IPageViewModel
		{
			return new OpenViewMessage
			{
				ViewInitializer = ConvertInitializer(initializer),
				ViewType = typeof(T),
				CanQueue = true
			};
		}

#if DEBUG
		public static OpenViewMessage ForDebug(string viewModel)
		{
			var fullName = $"ChelseaAppsFactory.Mobile.Common.{viewModel}";
			var viewModelType = Type.GetType(fullName);

			if (viewModelType == null)
			{
				throw new NullReferenceException($"Could not find {fullName}");
			}

			var method = typeof(OpenViewMessage).GetTypeInfo().DeclaredMethods.First(x => x.Name == "For" && x.IsGenericMethod);
			var generic = method.MakeGenericMethod(viewModelType);
			var message = generic.Invoke(null, new object[] { null });
			return (OpenViewMessage)message;
		}
#endif

		static Action<IPageViewModel> ConvertInitializer<T>(Action<T> initializer)
		{
			if (initializer == null)
			{
				return x => { };
			}

			return x =>
			{
				if (x is T)
				{
					initializer((T)x);
				}
				else
				{
					throw new InvalidOperationException("Invalid type on view initializer.");
				}
			};
		}

		protected OpenViewMessage()
		{
		}

		public Type ViewType { get; private set; }

		public Action<IPageViewModel> ViewInitializer { get; private set; }

		public HistoryAction HistoryAction { get; set; }

		public bool CanQueue { get; private set; } = false;
	}
}
