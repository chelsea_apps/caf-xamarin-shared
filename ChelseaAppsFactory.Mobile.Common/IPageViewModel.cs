﻿using System.Threading.Tasks;

namespace ChelseaAppsFactory.Mobile.Common
{
	public interface IPageViewModel 
	{
		void CleanUp();

		Task InitAsync();

		Task OnAppearingAsync();

		Task OnDisappearingAsync();
	}
}