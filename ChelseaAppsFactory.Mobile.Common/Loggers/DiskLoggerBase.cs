﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace ChelseaAppsFactory.Mobile.Common.Loggers
{
    /// <summary>
    /// Implementation of ILogger that writes logs to StreamWriter-based output, ideally to some persistent storage. 
    /// </summary>
    public abstract class DiskLoggerBase : ILogger, IDisposable
    {
        protected string DiskLogFileName;
        protected volatile StreamWriter DiskLogStream;
        readonly Func<string, StreamWriter> _createLogStreamFunc;

        protected DiskLoggerBase(string rootLogPath, Func<string, StreamWriter> createLogStreamFunc)
        {
            // init disk log
            if(string.IsNullOrEmpty(rootLogPath))
                Logger.Warn("No root log path specified, not logging");
            else
                DiskLogFileName = Path.Combine(rootLogPath, "Logs", DateTime.UtcNow.ToString("yyyyMMdd-HHmmss") + ".txt");

            _createLogStreamFunc = createLogStreamFunc;
        }

        public void Dispose()
        {
            if(DiskLogStream != null)
            {
                try
                {
                    DiskLogStream.Flush();
                    DiskLogStream.Dispose();
                }
#pragma warning disable RECS0022 // A catch clause that catches System.Exception and has an empty body
                catch
#pragma warning restore RECS0022 // A catch clause that catches System.Exception and has an empty body
                {
                }
                DiskLogStream = null;
            }
        }

        readonly AsyncLock _locker = new AsyncLock();

        async Task WriteLogStringAsync(string s)
        {
            if(DiskLogStream == null)
                return;

            await DiskLogStream.WriteLineAsync(s);
            await DiskLogStream.FlushAsync();
        }

        public async Task WriteLogAsync(LogLevel logLevel, string message)
        {
            if(DiskLogFileName == null)
                return;

            using(await _locker.LockAsync())
            {
                // open log if necessary
                if(DiskLogStream == null)
                {
                    try
                    {
                        Logger.Info($"Opening disk log at {DiskLogFileName}");
                        DiskLogStream = _createLogStreamFunc(DiskLogFileName);
                    }
                    catch
                    {
                        DiskLogFileName = null;
                        Logger.Warn("Cannot log to stream");
                        return;
                    }
                }

                await WriteLogStringAsync($"{logLevel.ToString()[0]} {LogManager.CurrentThreadWithTime}: {message}");
            }
        }

    }

}

