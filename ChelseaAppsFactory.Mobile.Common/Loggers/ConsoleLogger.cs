﻿using System.Threading.Tasks;
using System.Text;
using System;

namespace ChelseaAppsFactory.Mobile.Common.Loggers
{
    /// <summary>
    /// Implementation of ILogger that writes exclusively to Console.WriteLine. Should not be used in release builds.
    /// </summary>
    public sealed class ConsoleLogger : ILogger
    {
        static readonly char[] _wrapWhitespace = new[] { ' ', '\r', '\n', '\t' };
        static readonly char[] _wrapBreakingCharacters = new[] { ' ', ',', '.', '?', '!', ':', ';', '-', '\n', '\r', '\t' };
        const int WordWrapMessageThreshold = 1000;

        string WrapLongLines(string text, int wrapLength)
        {
            // start the string with a wrapped prefix so it can be identified as modified text
            var sb = new StringBuilder("(wrapped) ");

            int currentIndex;
            var lastWrap = 0;
            do
            {
                // find the next point to break the line, based on specific breaking characters
                currentIndex = lastWrap + wrapLength > text.Length ? text.Length : (text.LastIndexOfAny(_wrapBreakingCharacters, Math.Min(text.Length - 1, lastWrap + wrapLength)) + 1);
                if(currentIndex <= lastWrap)
                    currentIndex = Math.Min(lastWrap + wrapLength, text.Length);
                // append to the new string, with another space after the linebreak, to show it is wrapped
                sb.AppendLine(text.Substring(lastWrap, currentIndex - lastWrap).Trim(_wrapWhitespace)).Append(" ");
                lastWrap = currentIndex;
            } while(currentIndex < text.Length);

            return sb.ToString();
        }

        public Task WriteLogAsync(LogLevel logLevel, string message)
        {
            // to try and avoid slowing down Visual Studio's scrolling in the application console, 
            // word wrap the message, if it is over the maximum threshold
            if(message?.Length > WordWrapMessageThreshold)
            {
                message = WrapLongLines(message, WordWrapMessageThreshold);
            }

            // writes something like:
            //  I T1 2016-07-25 15:45:51.454: This is an informational message
            Console.WriteLine($"{logLevel.ToString()[0]} {LogManager.CurrentThreadWithTime}: {message}");
            return Task.FromResult(true);
        }
    }
}

