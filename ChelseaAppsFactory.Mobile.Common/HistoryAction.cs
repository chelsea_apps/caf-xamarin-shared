﻿using System;

namespace ChelseaAppsFactory.Mobile.Common
{
	public abstract class HistoryAction
	{
		public static HistoryAction ClearAll => new ClearAllHistoryAction();
		public static HistoryAction ToPage(Type type) => new ClearToPageHistoryAction(type);

		public class ClearAllHistoryAction : HistoryAction { }
		public class ClearToPageHistoryAction : HistoryAction
		{
			public ClearToPageHistoryAction(Type clearToType)
			{
				ClearToType = clearToType;
			}

			public Type ClearToType { get; }
		}
	}

}
