﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChelseaAppsFactory.Mobile.Common
{
    /// <summary>
    /// Manages writing to logs. If no loggers are registered, does nothing.
    /// </summary>
    public sealed class LogManager
    {
        readonly List<ILogger> _loggers;

        public LogManager()
        {
#if !DEBUG
            CurrentLogLevel = LogLevel.Warn;
#endif
            _loggers = new List<ILogger>();
        }

        /// <summary>
        /// Registers logging implementations with the manager.
        /// </summary>
        /// <param name="loggerList">Logger(s) to register.</param>
        public void RegisterLoggers(params ILogger[] loggerList)
        {
            if(loggerList == null)
                return;

            foreach(var logger in loggerList)
            {
                // check if this type of logger already registered
                if(logger == null || _loggers.Any(l => l.GetType() == logger.GetType()))
                    return;

                _loggers.Add(logger);
            }
        }

        /// <summary>
        /// Gets or sets the current logging level. Defaults to 'Debug' or 'Warn' in release configuration.
        /// </summary>
        public LogLevel CurrentLogLevel
        {
            get;
            set;
        }

        /// <summary>
        /// Writes a single message to all registered loggers.
        /// </summary>
        /// <param name="level">Log level to write as.</param>
        /// <param name="message">Message to display.</param>
        internal async Task WriteLogAsync(LogLevel level, string message)
        {
            // call loggers with message asynchronously
            await Task.WhenAll(from l in _loggers select l.WriteLogAsync(level, message));
        }

        //
        // General methods for logging, these are internal to enforce being called from Logger only.
        //

        internal async Task Debug(string message)
        {
            if(CurrentLogLevel <= LogLevel.Debug)
                await WriteLogAsync(LogLevel.Debug, message);
        }

        internal async Task Info(string message)
        {
            if(CurrentLogLevel <= LogLevel.Info)
                await WriteLogAsync(LogLevel.Info, message);
        }

        internal async Task Warn(string message)
        {
            if(CurrentLogLevel <= LogLevel.Warn)
                await WriteLogAsync(LogLevel.Warn, message);
        }

        internal async Task Crit(string message)
        {
            if(CurrentLogLevel <= LogLevel.Crit)
                await WriteLogAsync(LogLevel.Crit, message);
        }

        internal async Task Exception(Exception ex)
        {
            // always write exceptions (TODO: yes/no?)
            await WriteLogAsync(LogLevel.Exception, ex.ToString());
        }

        //
        // Utility methods for use with ILogger
        //

        /// <summary>
        /// Gets the current time with fractions of a second, in a sortable format.
        /// </summary>
        public static string CurrentTime
        {
            get { return DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"); }
        }

        /// <summary>
        /// Gets the current thread number with the time.
        /// </summary>
        public static string CurrentThreadWithTime
        {
            get { return $"T{Environment.CurrentManagedThreadId} {CurrentTime}"; }
        }

        /// <summary>
        /// Gets the current thread ID from the system. 1 is main thread.
        /// </summary>
        public static string CurrentThread
        {
            get { return $"T{Environment.CurrentManagedThreadId}"; }
        }
    }

}

