﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Reflection;

namespace ChelseaAppsFactory.Mobile.Common
{
    /// <summary>
    /// Log message severity.
    /// </summary>
    public enum LogLevel
    {
        Debug,
        Info,
        Warn,
        Crit,
        Exception
    }

    /// <summary>
    /// Interface for implementing writing to a log.
    /// </summary>
    public interface ILogger
    {
        Task WriteLogAsync(LogLevel logLevel, string message);
    }

    /// <summary>
    /// Top-level class for writing to the log.
    /// </summary>
    public static class Logger
    {
        static readonly LogManager _currentManager = new LogManager();

        public static event EventHandler<ExceptionEventArgs> ExceptionRaised;

        /// <summary>
        /// Writes a message to the debug log.
        /// </summary>
        /// <param name="message">Message to write.</param>
        [Obsolete("Logger.Debug usage should be removed")] // we mark this in the compile log as debug messages shouldn't appear in production
        public static void Debug(string message)
        {
            _currentManager.Debug(message).LogIfFaulted();
        }

        /// <summary>
        /// Writes a message to the informational log.
        /// </summary>
        /// <param name="message">Message to write.</param>
        public static void Info(string message)
        {
            _currentManager.Info(message).LogIfFaulted();
        }

        /// <summary>
        /// Writes a message to the warning log.
        /// </summary>
        /// <param name="message">Message to write.</param>
        public static void Warn(string message)
        {
            _currentManager.Warn(message).LogIfFaulted();
        }

        /// <summary>
        /// Writes a message to the critical log.
        /// </summary>
        /// <param name="message">Message to write.</param>
        public static void Crit(string message)
        {
            _currentManager.Crit(message).LogIfFaulted();
        }

        /// <summary>
        /// Writes a message to the exception log.
        /// </summary>
        /// <param name="ex">Exception to log.</param>
        public static void Exception(Exception ex)
        {
            _currentManager.Exception(ex).LogIfFaulted();
            // additionally raise event to make interested parties aware of an exception occurring
            // note that this should not replace other exception handlers
            ExceptionRaised?.Invoke(null, new ExceptionEventArgs(ex));
        }

        /// <summary>
        /// Logs if the passed in Task faults
        /// </summary>
        public static void LogIfFaulted(this Task task, [CallerMemberName] string callerName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            task.ContinueWith(t => {
                if(t.Exception != null)
                {
                    Crit($"Background task failed at {callerName} in {sourceFilePath}:{sourceLineNumber}:\n\n\t{task.Exception}");
                }
            },
            TaskContinuationOptions.OnlyOnFaulted);
        }

        /// <summary>
        /// Sets the current log level
        /// </summary>
        /// <param name="logLevel">The new logging level</param>
        public static void SetLogLevel(LogLevel logLevel)
        {
            _currentManager.CurrentLogLevel = logLevel;
        }

        /// <summary>
        /// Registers new logging interfaces with the log manager.
        /// </summary>
        /// <param name="loggers">List of loggers to register. Null is ignored.</param>
        public static void RegisterLoggers(params ILogger[] loggers)
        {
            _currentManager.RegisterLoggers(loggers);
        }

        /// <summary>
        /// Utility to log when a property changes.
        /// </summary>
        [Obsolete("Logger.DebugPropertyChanged should not be used in production code")]
        public static void DebugPropertyChanged(INotifyPropertyChanged target, string property)
        {
            target.PropertyChanged += (sender, e) => {
                if(e.PropertyName == property)
                {
                    var value = target.GetType().GetTypeInfo().GetDeclaredProperty(property).GetValue(target, null);
#pragma warning disable CS0618 // Type or member is obsolete
                    Debug($"{target.GetType().Name}.{property} changed to '{value}'");
#pragma warning restore CS0618 // Type or member is obsolete
                }
            };
        }

        public class ExceptionEventArgs : EventArgs
        {
            internal ExceptionEventArgs(Exception e)
            {
                RaisedException = e;
            }

            public Exception RaisedException
            {
                get;
                private set;
            }
        }
    }
}

