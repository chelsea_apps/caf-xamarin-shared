﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ChelseaAppsFactory.Mobile.Common
{
    public abstract class NotifyPropertyChangedBase : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		protected bool PropertyEventsEnabled { get; set; } = true;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyEventsEnabled)
			{
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		protected virtual void OnPropertyChanged(params string[] propertyNames)
		{
			if (PropertyEventsEnabled)
			{
                foreach (var propertyName in propertyNames)
                {
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
                }
			}
		}

        protected virtual void ForwardChangeEvent(string from, params string[] to)
		{
			if (to != null)
			{
				PropertyChanged += (s, e) =>
				{
					if (e.PropertyName == from)
					{
						foreach (var destination in to)
						{
							OnPropertyChanged(destination);
						}
					}
				};
			}
		}

		protected bool SetProperty<T>(ref T field, T value, [CallerMemberName] string whichProperty = null, params string[] otherProperties) where T : class
		{
			if (field == null && value == null)
			{
				return false;
			}
			if (field != null && field.Equals(value))
			{
				return false;
			}
			field = value;
			OnPropertyChanged(whichProperty);
			foreach (var property in otherProperties)
			{
				OnPropertyChanged(property);
			}
			return true;
		}

		protected bool SetProperty<T>(ref T? field, T? value, [CallerMemberName] string whichProperty = null, params string[] otherProperties) where T : struct
		{
			if (field == null && value == null)
			{
				return false;
			}
			if (field != null && field.Equals(value))
			{
				return false;
			}
			field = value;
			OnPropertyChanged(whichProperty);
			foreach (var property in otherProperties)
			{
				OnPropertyChanged(property);
			}
			return true;
		}

        // The enum overload won't be used unless an explicitly different name is used.

		protected bool SetEnumProperty<T>(ref T field, T value, [CallerMemberName] string whichProperty = null, params string[] otherProperties)
		{
			if (field.Equals(value))
			{
				return false;
			}
			field = value;
			OnPropertyChanged(whichProperty);
			foreach (var property in otherProperties)
			{
				OnPropertyChanged(property);
			}
			return true;
		}

		// The methods below are to avoid boxing in field.Equals. Versions for other value types could be added

		protected bool SetProperty(ref int field, int value, [CallerMemberName] string whichProperty = null, params string[] otherProperties)
		{
			if (field == value)
			{
				return false;
			}
			field = value;
			OnPropertyChanged(whichProperty);
			foreach (var property in otherProperties)
			{
				OnPropertyChanged(property);
			}
			return true;
		}

		protected bool SetProperty(ref bool field, bool value, [CallerMemberName] string whichProperty = null, params string[] otherProperties)
		{
			if (field == value)
			{
				return false;
			}
			field = value;
			OnPropertyChanged(whichProperty);
			foreach (var property in otherProperties)
			{
				OnPropertyChanged(property);
			}
			return true;
		}

		protected bool SetProperty(ref decimal field, decimal value, [CallerMemberName] string whichProperty = null, params string[] otherProperties)
		{
			if (field == value)
			{
				return false;
			}
			field = value;
			OnPropertyChanged(whichProperty);
			foreach (var property in otherProperties)
			{
				OnPropertyChanged(property);
			}
			return true;
		}

		protected bool SetProperty(ref DateTimeOffset field, DateTimeOffset value, [CallerMemberName] string whichProperty = null, params string[] otherProperties)
		{
			if (field == value)
			{
				return false;
			}
			field = value;
			OnPropertyChanged(whichProperty);
			foreach (var property in otherProperties)
			{
				OnPropertyChanged(property);
			}
			return true;
		}

		protected bool SetProperty(ref DateTime field, DateTime value, [CallerMemberName] string whichProperty = null, params string[] otherProperties)
		{
			if (field == value)
			{
				return false;
			}
			field = value;
			OnPropertyChanged(whichProperty);
			foreach (var property in otherProperties)
			{
				OnPropertyChanged(property);
			}
			return true;
        }

        protected bool SetProperty(ref TimeSpan field, TimeSpan value, [CallerMemberName] string whichProperty = null, params string[] otherProperties)
        {
            if (field == value)
            {
                return false;
            }
            field = value;
            OnPropertyChanged(whichProperty);
            foreach (var property in otherProperties)
            {
                OnPropertyChanged(property);
            }
            return true;
        }
    }
}
