﻿using System;
using System.Collections.Generic;
using System.Linq;

using ChelseaAppsFactory.Mobile.Common;
using Foundation;
using UIKit;

namespace ChelseaAppsFactory.Mobile.TestApp.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Logger.RegisterLoggers(new Common.Loggers.ConsoleLogger());

            global::Xamarin.Forms.Forms.Init();
            LoadApplication(new App());



            return base.FinishedLaunching(app, options);
        }
    }
}
