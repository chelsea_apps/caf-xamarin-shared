﻿using System.ComponentModel;
using Foundation;
using UIKit;
using ChelseaAppsFactory.Mobile.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using ChelseaAppsFactory.Mobile.Renderers;

[assembly: ExportRenderer(typeof(Link), typeof(LinkRenderer))]
namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class LinkRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if(e.NewElement != null)
            {
                e.NewElement.PropertyChanged += Element_PropertyChanged;
                UpdateUnderline();
            }

            if(e.OldElement != null)
            {
                e.OldElement.PropertyChanged -= Element_PropertyChanged;
            }
        }

        void Element_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(Element.Text)
               || e.PropertyName == nameof(Element.FormattedText)
               || e.PropertyName == nameof(Element.FontSize)
               || e.PropertyName == nameof(Element.FontFamily)
               || e.PropertyName == nameof(Element.TextColor)
              )
            {
                UpdateUnderline();
            }
        }

        void UpdateUnderline()
        {
            Control.SetUnderlineStyle(NSUnderlineStyle.Single);
        }
    }
}