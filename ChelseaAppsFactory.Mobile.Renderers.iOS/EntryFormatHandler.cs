using System;
using Foundation;
using UIKit;
using ChelseaAppsFactory.Mobile.Controls;

namespace ChelseaAppsFactory.Mobile.Renderers
{
    public static class EntryFormatHandler
    {
        /// <summary>
        /// Helper for setting formatted text in a UITextField, that figures out where the cursor should end up
        /// </summary>
        /// <param name="field">The field to change</param>
        /// <param name="range">Original range replacement</param>
        /// <param name="replacementString">Original replacement string, before formatting</param>
        public static void HandleFormattedChange(this UITextField field, NSRange range, string replacementString, FlatEntry entry)
        {
            var formatter = entry.Formatter;
            var oldCursorPosition = field.GetCursorPosition();

            var changedText = ReplaceRange(field.Text, range, replacementString);
            var formattedText = formatter.Convert(changedText, null, null, null);

            entry.Text = entry.Formatter.ConvertBack(formattedText, null, null, null);
            field.Text = formattedText;

            if(!field.Text.Contains(replacementString))
            {
                // Change was removed by formatter
                return;
            }

            var delta = GetOffset(range, replacementString);
            var newPosition = ClampCursorPosition(field, oldCursorPosition + delta);

            if(replacementString.Length == 1)
            {
                // One character inserted, check that cursor is in correct position
                // Formatter may have added chars before the cursor

                var rangeBeforeCursor = RangeBeforeCursor(field, newPosition, replacementString.Length);
                if(rangeBeforeCursor != replacementString)
                {
                    // Cursor is in the wrong position
                    newPosition += 1;
                }
            }

            field.SetCursorPosition(newPosition);
        }

        static string ReplaceRange(string text, NSRange range, string replacement)
        {
            return text.Remove((int)range.Location, (int)range.Length).Insert((int)range.Location, replacement);
        }

        static string RangeBeforeCursor(UITextField textField, int cursorPosition, int length)
        {
            int start = Math.Max(0, cursorPosition - length);
            int end = Math.Min(textField.Text.Length - 1, length);
            return textField.Text.Substring(start, end);
        }

        public static int GetCursorPosition(this UITextField textField)
        {
            return (int)textField.GetOffsetFromPosition(textField.BeginningOfDocument, textField.SelectedTextRange.Start);
        }

        static void SetCursorPosition(this UITextField field, int position)
        {
            position = ClampCursorPosition(field, position);
            var newCursorPos = field.GetPosition(field.BeginningOfDocument, position);
            var newRange = field.GetTextRange(newCursorPos, newCursorPos);
            field.SelectedTextRange = newRange;
        }

        static int ClampCursorPosition(UITextField field, int position)
        {
            return Math.Min(Math.Max(0, position), field.Text.Length);
        }

        static int GetOffset(NSRange range, string replacementString)
        {
            return (int)(replacementString.Length - range.Length);
        }
    }
}