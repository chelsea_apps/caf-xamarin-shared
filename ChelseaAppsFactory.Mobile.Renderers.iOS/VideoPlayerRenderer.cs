﻿using System;
using System.IO;
using AVFoundation;
using CoreGraphics;
using Foundation;
using UIKit;
using ChelseaAppsFactory.Mobile.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(VideoPlayer), typeof(ChelseaAppsFactory.Mobile.Renderers.VideoPlayerRenderer))]
namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class VideoPlayerRenderer : ViewRenderer
    {
        AVPlayer player;
        AVPlayerItem item;
        AVPlayerLayer layer;
        AVAsset asset;

        IDisposable playToEndObserver;
        IDisposable didBecomeActiveObserver;

        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            if(e.NewElement == null)
                return;
            
            base.OnElementChanged(e);
            
            didBecomeActiveObserver = NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.DidBecomeActiveNotification, (obj) => 
            {
                if (player != null)
                    player.Play();
            });

            NativeView.BackgroundColor = UIColor.Clear;

            SetupPlayer();

            Element.PropertyChanged += (sender, args) =>
            {
                if(args.PropertyName == nameof(VideoPlayer.FileName))
                {
                    SetupPlayer();
                }
            };
        }

        void SetupPlayer()
        {
            var filename = (Element as VideoPlayer)?.FileName;

            if(string.IsNullOrWhiteSpace(filename))
                return;
                
            var newAsset = AVAsset.FromUrl(new NSUrl(Path.Combine(NSBundle.MainBundle.BundlePath, filename), false));
            var newItem = new AVPlayerItem(newAsset);
            var newPlayer = new AVPlayer(newItem);

            var newPlayToEndObserver = NSNotificationCenter.DefaultCenter.AddObserver(AVPlayerItem.DidPlayToEndTimeNotification, (obj) =>
            {
                player.Seek(CoreMedia.CMTime.Zero);
                player.Play();
            }, newItem);

            var newLayer = AVPlayerLayer.FromPlayer(newPlayer);
            newLayer.BackgroundColor = UIColor.Clear.CGColor;
            newLayer.Frame = GetLayerFrame();
            newLayer.ShouldRasterize = true;

            if(player != null)
            {
                NativeView.Layer.ReplaceSublayer(layer, newLayer);

                layer.Dispose();
                playToEndObserver.Dispose();
                player.Dispose();
                item.Dispose();
                asset.Dispose();
            }
            else
            {
                NativeView.Layer.AddSublayer(newLayer);
            }

            asset = newAsset;
            item = newItem;
            player = newPlayer;
            playToEndObserver = newPlayToEndObserver;
            layer = newLayer;

            (Element as IVisualElementController).NativeSizeChanged();
        }

        public override SizeRequest GetDesiredSize(double widthConstraint, double heightConstraint)
        {
            if(asset == null)
                return base.GetDesiredSize(widthConstraint, heightConstraint);
            
            return new SizeRequest(new Size(asset.NaturalSize.Width, asset.NaturalSize.Height));
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            layer.Frame = GetLayerFrame();
            player.Play();
        }

        CGRect GetLayerFrame()
        {
            return new CGRect(0, 0, NativeView.Frame.Width, NativeView.Frame.Height);
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                playToEndObserver.Dispose();
                didBecomeActiveObserver.Dispose();
                player.Dispose();
                item.Dispose();
                asset.Dispose();
                layer.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
