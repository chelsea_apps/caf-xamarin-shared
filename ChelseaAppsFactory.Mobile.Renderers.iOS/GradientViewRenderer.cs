﻿using System.ComponentModel;
using System.Linq;
using CoreAnimation;
using CoreGraphics;
using UIKit;
using ChelseaAppsFactory.Mobile.Renderers;
using ChelseaAppsFactory.Mobile.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(GradientView), typeof(GradientViewRenderer))]

namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class GradientViewRenderer : ViewRenderer<GradientView, UIView>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<GradientView> e)
        {
            base.OnElementChanged(e);

            if(e.NewElement != null)
            {
                e.NewElement.PropertyChanged += Element_PropertyChanged;
            }

            if(e.OldElement != null)
            {
                e.OldElement.PropertyChanged -= Element_PropertyChanged;
            }
        }

        void Element_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(GradientView.Colors))
            {
                SetNeedsDisplay();
            }
        }

        CAGradientLayer gradient;

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            if(Element == null)
            {
                return;
            }

            if(gradient == null)
            {
                gradient = CAGradientLayerForOrientation(Element.Orientation);
                Layer.InsertSublayer(gradient, 0);
            }

            gradient.Colors = Element.Colors.Select(x => x.ToCGColor()).ToArray();
            gradient.Frame = Bounds;
        }

        public override CGRect Frame
        {
            get
            {
                return base.Frame;
            }
            set
            {
                base.Frame = value;

                if(gradient != null)
                {
                    gradient.Frame = Bounds;
                }
            }
        }

        CAGradientLayer CAGradientLayerForOrientation(GradientOrientation orientation)
        {
            if(orientation == GradientOrientation.Horizontal)
            {
                return new CAGradientLayer
                {
                    StartPoint = new CGPoint(0, 0.5),
                    EndPoint = new CGPoint(1, 0.5)
                };
            }

            return new CAGradientLayer
            {
                StartPoint = new CGPoint(0.5, 0),
                EndPoint = new CGPoint(0.5, 1)
            };
        }
    }
}