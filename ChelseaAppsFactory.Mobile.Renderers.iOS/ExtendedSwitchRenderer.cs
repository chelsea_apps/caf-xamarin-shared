﻿using System;
using ChelseaAppsFactory.Mobile.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ExtendedSwitch), typeof(ChelseaAppsFactory.Mobile.Renderers.ExtendedSwitchRenderer))]

namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class ExtendedSwitchRenderer : SwitchRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Switch> e)
        {
            base.OnElementChanged(e);

            var es = e.NewElement as ExtendedSwitch;
            if(es != null && Control != null)
                Control.OnTintColor = es.TintColor.ToUIColor();
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            var es = Element as ExtendedSwitch;
            if(es != null)
            {
                if(e.PropertyName == ExtendedSwitch.TintColorProperty.PropertyName)
                {
                    Control.OnTintColor = es.TintColor.ToUIColor();
                }
            }
        }
    }
}

