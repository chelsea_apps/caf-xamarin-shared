﻿using System.ComponentModel;
using ChelseaAppsFactory.Mobile.Controls;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(FlatButton), typeof(ChelseaAppsFactory.Mobile.Renderers.FlatButtonRenderer))]
namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class FlatButtonRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            if(e.NewElement != null)
            {
                e.NewElement.PropertyChanged += Element_PropertyChanged;
                UpdateDisabledTextColor();
            }

            if(e.OldElement != null)
            {
                e.OldElement.PropertyChanged -= Element_PropertyChanged;
            }
        }

        void Element_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(Element.TextColor))
            {
                UpdateDisabledTextColor();
            }
        }

        void UpdateDisabledTextColor()
        {
            var textColor = Element.TextColor.ToUIColor();
            Control.SetTitleColor(textColor, UIControlState.Disabled);
        }
    }
}