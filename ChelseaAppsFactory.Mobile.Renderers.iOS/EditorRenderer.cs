﻿using System;
using ChelseaAppsFactory.Mobile.Controls;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ExtendedEditor), typeof(ChelseaAppsFactory.Mobile.Renderers.EditorRenderer))]
namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class EditorRenderer : Xamarin.Forms.Platform.iOS.EditorRenderer
    {
        UILabel placeholder;
        ExtendedEditor element => Element as ExtendedEditor;

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null && element != null && Control != null)
            {
                Control.InputAccessoryView = null;

                Control.ShouldChangeText = (textView, range, text) => 
                {
                    var newCount = textView.Text.Length + text.Length - range.Length;
                    
                    if(newCount > 0)
                        placeholder.Hidden = true;
                    else
                        placeholder.Hidden = false;
                    
                    return newCount <= element.MaxCharacters;
                };
                
                placeholder = new UILabel();
                Control.AddSubview(placeholder);

                UpdatePlaceholderFont();
                UpdatePlaceholder();
            }
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == ExtendedEditor.PlaceholderFontSizeProperty.PropertyName || 
                e.PropertyName == ExtendedEditor.PlaceholderFontFamilyProperty.PropertyName || 
                e.PropertyName == ExtendedEditor.PlaceholderFontSizeProperty.PropertyName)
            {
                UpdatePlaceholderFont();
            }
            else if (e.PropertyName == ExtendedEditor.PlaceholderProperty.PropertyName)
            {
                UpdatePlaceholder();
            }
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            placeholder.SizeToFit();

            var frame = placeholder.Frame;
            frame.Location = new CoreGraphics.CGPoint(6, 7);
            placeholder.Frame = frame;
        }

        void UpdatePlaceholderFont()
        {
            var font = UIFont.FromName(element.PlaceholderFontFamily, element.PlaceholderFontSize);

            // SFUIText is the system font, and therefore is named .SFUIText.
            // TODO: We should make this smarter, in case the system font ever changes.
            if(font == null)
                font = UIFont.FromName($".{element.PlaceholderFontFamily}", element.PlaceholderFontSize);

            if(font != null) // Attempting to set the font to null causes a crash
                placeholder.Font = font;

            placeholder.TextColor = element.PlaceholderTextColour.ToUIColor();
        }

        void UpdatePlaceholder()
        {
            placeholder.Text = element.Placeholder;
        }
    }
}
