﻿using System;
using System.Threading.Tasks;
using Foundation;
using ChelseaAppsFactory.Mobile.Renderers;
using ChelseaAppsFactory.Mobile.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ScriptableWebView), typeof(ScriptableWebViewRenderer))]

namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class ScriptableWebViewRenderer : WebViewRenderer
    {
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            var webView = e.NewElement as ScriptableWebView;
            if(webView != null)
            {
                NSDictionary dictionary = NSDictionary.FromObjectAndKey(NSObject.FromObject(webView.UserAgent), NSObject.FromObject("UserAgent"));
                NSUserDefaults.StandardUserDefaults.RegisterDefaults(dictionary);
                webView.SetExecuteJavaScriptFunc((js) =>
                {
                    return Task.FromResult(EvaluateJavascript(js));
                });
#if DEBUG
                webView.Navigating += (sender, ee) => Common.Logger.Debug($"WebView: Navigating {ee.Url}");
#endif
            }
        }
    }
}
