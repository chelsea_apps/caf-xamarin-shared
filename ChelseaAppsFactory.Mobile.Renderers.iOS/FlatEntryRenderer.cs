﻿using System;
using Foundation;
using UIKit;
using ChelseaAppsFactory.Mobile.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System.ComponentModel;

[assembly: ExportRenderer(typeof(FlatEntry), typeof(ChelseaAppsFactory.Mobile.Renderers.FlatEntryRenderer))]

namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class FlatEntryRenderer : EntryRenderer
    {
        FlatEntry Entry => Element as FlatEntry;

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            Control.BorderStyle = UITextBorderStyle.None;
            Control.ShouldChangeCharacters = HandleUITextFieldChange;
            Control.AutocorrectionType = UITextAutocorrectionType.No;
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if(UIDevice.CurrentDevice.CheckSystemVersion(11, 0))
            {
                // Work around iOS 11 bug: text color doesn't update when not focused
                if(e.PropertyName == nameof(Element.TextColor))
                {
                    Control.SetNeedsLayout();
                }
            }
        }

        bool HandleUITextFieldChange(UITextField textField, NSRange range, string replacementString)
        {
            if(WillExceedMaximumLength(range, replacementString))
            {
                return false;
            }

            if(Entry.Formatter != null)
            {
                textField.HandleFormattedChange(range, replacementString, Entry);
                return false;
            }

            return true;
        }

        bool WillExceedMaximumLength(NSRange range, string replacementString)
        {
            if(Entry.MaximumLength < 0)
            {
                return false;
            }

            var newLength = Control.Text.Length - range.Length + replacementString.Length;
            return (newLength > Entry.MaximumLength);
        }
    }
}