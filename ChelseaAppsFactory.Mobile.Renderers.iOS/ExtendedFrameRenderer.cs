﻿using System;
using System.ComponentModel;
using System.Drawing;
using ChelseaAppsFactory.Mobile.Controls;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

// This is mostly just the source renderer. Formatting and style left unchanged.

[assembly: ExportRenderer(typeof(ExtendedFrame), typeof(ChelseaAppsFactory.Mobile.Renderers.ExtendedFrameRenderer))]

namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class ExtendedFrameRenderer : FrameRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);

            if(e.NewElement != null)
                SetupLayer();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if(e.PropertyName == VisualElement.BackgroundColorProperty.PropertyName || e.PropertyName == Xamarin.Forms.Frame.OutlineColorProperty.PropertyName ||
               e.PropertyName == Xamarin.Forms.Frame.HasShadowProperty.PropertyName || e.PropertyName == ExtendedFrame.BorderRadiusProperty.PropertyName)
                SetupLayer();
        }

        void SetupLayer()
        {
            Layer.CornerRadius = (nfloat)(double)Element.GetValue(ExtendedFrame.BorderRadiusProperty);
            if(Element.BackgroundColor == Color.Default)
                Layer.BackgroundColor = UIColor.White.CGColor;
            else
                Layer.BackgroundColor = Element.BackgroundColor.ToUIColor().CGColor;

            if(Element.HasShadow)
            {
                Layer.ShadowRadius = 2;
                Layer.ShadowColor = UIColor.Black.CGColor;
                Layer.ShadowOpacity = 0.3f;
                Layer.ShadowOffset = new SizeF();
            }
            else
                Layer.ShadowOpacity = 0;

            if(Element.OutlineColor == Color.Default)
                Layer.BorderColor = UIColor.Clear.CGColor;
            else
            {
                Layer.BorderColor = Element.OutlineColor.ToCGColor();
                Layer.BorderWidth = 1;
            }

            Layer.RasterizationScale = UIScreen.MainScreen.Scale;
            Layer.ShouldRasterize = true;
        }
    }
}

