﻿using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables;
using ChelseaAppsFactory.Mobile.Renderers;
using ChelseaAppsFactory.Mobile.Controls;
using System;
using System.ComponentModel;
using Android.Content;

[assembly: ExportRenderer(typeof(GradientView), typeof(GradientViewRenderer))]

namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class GradientViewRenderer : ViewRenderer<GradientView, Android.Views.View>
    {
        GradientDrawable drawable;

        public GradientViewRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<GradientView> e)
        {
            base.OnElementChanged(e);

            if(e.NewElement != null)
            {
                e.NewElement.PropertyChanged += Element_PropertyChanged;
                SetDrawable();
            }

            if(e.OldElement != null)
            {
                e.OldElement.PropertyChanged -= Element_PropertyChanged;
            }
        }

        void Element_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(Element.Colors))
            {
                UpdateColors();
            }
        }

        void SetDrawable()
        {
            drawable = new GradientDrawable();
            drawable.SetOrientation(Orientation);
            Background = drawable;
            UpdateColors();
        }

        void UpdateColors()
        {
            if(drawable != null)
            {
                drawable.SetColors(Element.Colors.Select(x => x.ToAndroid().ToArgb()).ToArray());
            }
        }

        GradientDrawable.Orientation Orientation =>
            Element?.Orientation == GradientOrientation.Vertical
                ? GradientDrawable.Orientation.TopBottom
                : GradientDrawable.Orientation.LeftRight;
    }
}