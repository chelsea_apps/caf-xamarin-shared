﻿using System;
using Android.Content;
using Android.Graphics.Drawables;
using ChelseaAppsFactory.Mobile.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ExtendedSwitch), typeof(ChelseaAppsFactory.Mobile.Renderers.ExtendedSwitchRenderer))]

namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class ExtendedSwitchRenderer : SwitchRenderer, Android.Widget.CompoundButton.IOnCheckedChangeListener
    {
        public ExtendedSwitchRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Switch> e)
        {
            base.OnElementChanged(e);

            if(e.NewElement != null)
            {
                ApplyTintColour(e.NewElement as ExtendedSwitch);
            }
            Control.SetOnCheckedChangeListener(this);
        }

        void Android.Widget.CompoundButton.IOnCheckedChangeListener.OnCheckedChanged(Android.Widget.CompoundButton buttonView, bool isChecked)
        {
            ApplyTintColour(Element as ExtendedSwitch);
            ((IViewController)Element).SetValueFromRenderer(Switch.IsToggledProperty, isChecked);
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if(e.PropertyName == ExtendedSwitch.TintColorProperty.PropertyName)
            {
                ApplyTintColour(Element as ExtendedSwitch);
            }
        }

        void ApplyTintColour(ExtendedSwitch es)
        {
            if(es != null && Control != null)
            {
                // based on http://stackoverflow.com/a/25635526/2810461
                if(Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop) // 5.0
                {
                    Android.Graphics.Color color;
                    if(Control.Checked)
                    {
                        color = es.TintColor.ToAndroid();
                    }
                    else
                    {
                        color = Android.Graphics.Color.Gray;
                    }

                    Control.ThumbDrawable.SetColorFilter(color, Android.Graphics.PorterDuff.Mode.Multiply);
                    Control.TrackDrawable.SetColorFilter(color, Android.Graphics.PorterDuff.Mode.Multiply);
                }
                else
                {
                    var stl = new StateListDrawable();
                    stl.AddState(new int[] { Android.Resource.Attribute.StateChecked }, new ColorDrawable(es.TintColor.ToAndroid()));
                    Control.ThumbDrawable = stl;
                }
            }
        }
    }
}

