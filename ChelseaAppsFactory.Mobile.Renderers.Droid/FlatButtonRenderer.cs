﻿using System;
using System.ComponentModel;
using Android.Content;
using ChelseaAppsFactory.Mobile.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(FlatButton), typeof(ChelseaAppsFactory.Mobile.Renderers.FlatButtonRenderer))]

namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class FlatButtonRenderer : ButtonRenderer
    {
        public FlatButtonRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                if(Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop)
                    Control.Elevation = 0;
                else
                    Android.Support.V4.View.ViewCompat.SetElevation(Control, 0);

                UpdateDisabledTextColor();
            }

            if(e.NewElement != null)
            {
                e.NewElement.PropertyChanged += Element_PropertyChanged;
            }

            if(e.OldElement != null)
            {
                e.OldElement.PropertyChanged -= Element_PropertyChanged;
            }
        }

        void Element_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(Element.TextColor))
            {
                UpdateDisabledTextColor();
            }
        }

        void UpdateDisabledTextColor()
        {
            Control.SetTextColor(Element.TextColor.ToAndroid());
        }
    }
}


