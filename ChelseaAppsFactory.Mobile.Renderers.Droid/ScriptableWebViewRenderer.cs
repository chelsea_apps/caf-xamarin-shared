﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android.Webkit;
using ChelseaAppsFactory.Mobile.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Views;
using Android.Content;
using ChelseaAppsFactory.Mobile.Common;

[assembly: ExportRenderer(typeof(ScriptableWebView), typeof(ChelseaAppsFactory.Mobile.Renderers.ScriptableWebViewRenderer))]

namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class ScriptableWebViewRenderer : WebViewRenderer
    {
        AsyncLock locker = new AsyncLock();

        public ScriptableWebViewRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.WebView> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement is ScriptableWebView webView)
            {
                webView.SetExecuteJavaScriptFunc(ExecuteJavaScript);
            }
        }

        protected override Android.Webkit.WebView CreateNativeControl()
        {
            // Disable hardware rendering to prevent display issues with hosted payment page
            var control = base.CreateNativeControl();
            control.SetLayerType(LayerType.Software, null);
            return control;
        }

        async Task<string> ExecuteJavaScript(string script)
        {
            using(await locker.LockAsync())
            {
                return await Control?.EvaluateJavascriptAsync(script);
            }
        }
    }

    static class WebViewExtension
    {
        public static Task<string> EvaluateJavascriptAsync(this Android.Webkit.WebView webView, string script)
        {
            var callback = new JavascriptCallback();
            webView.EvaluateJavascript(script, callback);
            return callback.Result;
        }

        class JavascriptCallback : Java.Lang.Object, IValueCallback
        {
            TaskCompletionSource<string> tcs = new TaskCompletionSource<string>();
            public Task<string> Result => tcs.Task;

            public void OnReceiveValue(Java.Lang.Object value)
            {
                var trimmed = Convert.ToString(value).Trim('"');
                tcs.TrySetResult(trimmed);
            }
        }
    }
}
