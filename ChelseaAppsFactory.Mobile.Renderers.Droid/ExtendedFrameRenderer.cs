﻿using System;
using System.ComponentModel;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Support.V7.Widget;
using Android.Views;
using ChelseaAppsFactory.Mobile.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ExtendedFrame), typeof(ChelseaAppsFactory.Mobile.Renderers.ExtendedFrameRenderer))]

namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class ExtendedFrameRenderer : Xamarin.Forms.Platform.Android.AppCompat.FrameRenderer
    {
        public ExtendedFrameRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);

            UpdateBorderRadius();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if(e.PropertyName == ExtendedFrame.BorderRadiusProperty.PropertyName)
            {
                UpdateBorderRadius();
            }
        }

        void UpdateBorderRadius()
        {
            if(Element != null)
            {
                Control.Radius = (float)(double)Element.GetValue(ExtendedFrame.BorderRadiusProperty);
            }
        }

        bool disposed;

        protected override void Dispose(bool disposing)
        {
            if(disposing && !disposed)
            {
                disposed = true;

                if(Element != null)
                {
                    Element.PropertyChanged -= OnElementPropertyChanged;
                }
            }
            base.Dispose(disposing);
        }
    }
}

