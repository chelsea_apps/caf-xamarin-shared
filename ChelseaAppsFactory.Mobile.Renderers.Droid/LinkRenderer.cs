﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using ChelseaAppsFactory.Mobile.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Link), typeof(ChelseaAppsFactory.Mobile.Renderers.LinkRenderer))]
namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class LinkRenderer : LabelRenderer
    {
        public LinkRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if(Control != null)
            {
                UpdateUnderline();
            }
        }

        void UpdateUnderline()
        {
            Control.PaintFlags |= PaintFlags.UnderlineText;
        }
    }
}

