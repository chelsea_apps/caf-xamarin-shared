﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Media;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using ChelseaAppsFactory.Mobile.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(VideoPlayer), typeof(ChelseaAppsFactory.Mobile.Renderers.VideoPlayerRenderer))]
namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class VideoPlayerRenderer : ViewRenderer<VideoPlayer, SurfaceView>
    {
        MediaPlayer player;
        SurfaceView view;
        Context context;

        bool playerPrepared;
        bool surfaceInitialised;

        int width;
        int height;

        public VideoPlayerRenderer(Context context) : base(context)
        {
            this.context = context;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<VideoPlayer> e)
        {
            base.OnElementChanged(e);

            if(e.OldElement != null)
            {
                player?.Release();
                player?.Dispose();
                player = null;
                player.VideoSizeChanged -= Player_VideoSizeChanged;
            }

            if(e.NewElement != null)
            {
                player = new MediaPlayer();
                player.VideoSizeChanged += Player_VideoSizeChanged;

                view = new SurfaceView(context);
                view.Holder.AddCallback(new SurfaceHolderListener(this));

                SetupPlayer();

                SetNativeControl(view);
            }
        }

        void Player_VideoSizeChanged(object sender, MediaPlayer.VideoSizeChangedEventArgs e)
        {
            width = e.Width;
            height = e.Height;
            (Element as IVisualElementController).NativeSizeChanged();
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if(e.PropertyName == nameof(Element.FileName))
            {
                if (player.IsPlaying)
                    player.Reset();

                playerPrepared = false;

                SetupPlayer();
            }
        }

        public override SizeRequest GetDesiredSize(int widthConstraint, int heightConstraint)
        {
            if(width <= 0 || height <= 0)
                return base.GetDesiredSize(widthConstraint, heightConstraint);

            var ratio = (float)width / (float)height;
            Xamarin.Forms.Size size;

            if(widthConstraint < 0 || heightConstraint < 0)
                size = CalculateSize(Control.Width, Control.Height, ratio);
            else
                size = CalculateSize(widthConstraint, heightConstraint, ratio);

            return new SizeRequest(size);
        }

        Xamarin.Forms.Size CalculateSize(int widthConstraint, int heightConstraint, float ratio)
        {
            if(widthConstraint < heightConstraint)
            {
                return new Xamarin.Forms.Size(widthConstraint, widthConstraint * ratio);
            }
            else
            {
                return new Xamarin.Forms.Size(heightConstraint * ratio, heightConstraint);
            }
        }

        void SetupPlayer()
        {
            if(string.IsNullOrWhiteSpace(Element.FileName))
                return;

            var afd = context.Assets.OpenFd(Element.FileName);

            player.SetDataSource(afd.FileDescriptor, afd.StartOffset, afd.Length);
            player.Looping = true;

            PrepareAndPlay();
        }

        void PrepareAndPlay()
        {
            if(string.IsNullOrWhiteSpace(Element.FileName) || !surfaceInitialised)
                return;

            player.SetDisplay(view.Holder);

            if(!playerPrepared)
            {
                // Only prepare the player the first time, not when coming back from the background.
                // Doing so will cause an exception.
                player.Prepare();
                playerPrepared = true;
            }

            player.Start();
        }

        void Stop()
        {
            player.Stop();
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                player?.Release();
                player?.Dispose();
            }

            base.Dispose(disposing);
        }

        class SurfaceHolderListener : Java.Lang.Object, ISurfaceHolderCallback
        {
            readonly VideoPlayerRenderer renderer;

            public SurfaceHolderListener(VideoPlayerRenderer renderer)
            {
                this.renderer = renderer;
            }

            public void SurfaceChanged(ISurfaceHolder holder, [GeneratedEnum] Format format, int width, int height)
            {
                // Don't care!
            }

            public void SurfaceCreated(ISurfaceHolder holder)
            {
                renderer.surfaceInitialised = true;
                renderer.playerPrepared = false;
                renderer.PrepareAndPlay();
            }

            public void SurfaceDestroyed(ISurfaceHolder holder)
            {
                // Don't care!
                renderer.Stop();
            }
        }
    }
}
