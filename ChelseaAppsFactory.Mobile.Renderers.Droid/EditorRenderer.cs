﻿using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Graphics;
using Android.Text;
using Android.Views;
using Android.Widget;
using ChelseaAppsFactory.Mobile.Controls;
using Java.Lang;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ExtendedEditor), typeof(ChelseaAppsFactory.Mobile.Renderers.EditorRenderer))]
namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class EditorRenderer : ViewRenderer<ExtendedEditor, EditorRenderer.CustomKeyboardEditText>, ITextWatcher
    {
        IEditorController ElementController => Element;

        public EditorRenderer(Context context) : base(context)
        {
            AutoPackage = false;
        }

        void ITextWatcher.AfterTextChanged(IEditable s)
        {
        }

        void ITextWatcher.BeforeTextChanged(ICharSequence s, int start, int count, int after)
        {
        }

        void ITextWatcher.OnTextChanged(ICharSequence s, int start, int before, int count)
        {
            if(string.IsNullOrEmpty(Element.Text) && s.Length() == 0)
            {
                return;
            }

            if(Element.Text != s.ToString())
            {
                ElementController.SetValueFromRenderer(Editor.TextProperty, s.ToString());
            }
        }

        protected override CustomKeyboardEditText CreateNativeControl()
        {
            return new CustomKeyboardEditText(Context, Element);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<ExtendedEditor> e)
        {
            base.OnElementChanged(e);

            if(e.NewElement != null)
            {
                var edit = Control;
                if(edit == null)
                {
                    edit = CreateNativeControl();

                    SetNativeControl(edit);
                    edit.AddTextChangedListener(this);
                }

                edit.SetSingleLine(false);
                edit.Gravity = GravityFlags.Top;
                edit.SetHorizontallyScrolling(false);
                
                UpdateText();
                UpdateFont();
                SetPlaceholder();
                SetMaxCharacters();

                Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
            }
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if(e.PropertyName == Editor.TextProperty.PropertyName)
            {
                UpdateText();
                UpdateFont();
            }
            else if(e.PropertyName == Editor.FontFamilyProperty.PropertyName || 
                    e.PropertyName == Editor.FontSizeProperty.PropertyName || 
                    e.PropertyName == Editor.TextColorProperty.PropertyName ||
                    e.PropertyName == Editor.FontAttributesProperty.PropertyName || 
                    e.PropertyName == ExtendedEditor.PlaceholderFontFamilyProperty.PropertyName || 
                    e.PropertyName == ExtendedEditor.PlaceholderFontSizeProperty.PropertyName || 
                    e.PropertyName == ExtendedEditor.PlaceholderTextColourProperty.PropertyName)
            {
                UpdateFont();
            }
            else if(e.PropertyName == ExtendedEditor.PlaceholderProperty.PropertyName)
            {
                SetPlaceholder();
            }
            else if(e.PropertyName == ExtendedEditor.PlaceholderProperty.PropertyName)
            {
                SetPlaceholder();
            }
        }

        void UpdateText()
        {
            string newText = Element.Text ?? "";

            if(Control.Text == newText)
            {
                return;
            }

            Control.Text = newText;
            Control.SetSelection(newText.Length);
        }

        void UpdateFont()
        {
            if(Control.Text.Length > 0)
            {
                var font = Font.OfSize(Element.FontFamily, Element.FontSize);
                Control.SetTypeface(font.ToTypeface(), TypefaceStyle.Normal);
                Control.SetTextSize(Android.Util.ComplexUnitType.Sp, font.ToScaledPixel());
                Control.SetTextColor(Element.TextColor.ToAndroid());
            }
            else
            {
                var font = Font.OfSize(Element.PlaceholderFontFamily, Element.PlaceholderFontSize);
                Control.SetTypeface(font.ToTypeface(), TypefaceStyle.Normal);
                Control.SetTextSize(Android.Util.ComplexUnitType.Sp, font.ToScaledPixel());
                Control.SetTextColor(Element.PlaceholderTextColour.ToAndroid());
            }
        }

        void SetPlaceholder()
        {
            Control.Hint = Element.Placeholder;
        }

        void SetMaxCharacters()
        {
            if(Element.MaxCharacters > -1)
            {
                var filters = Control.GetFilters();
                var filterList = filters == null ? new List<IInputFilter>() : new List<IInputFilter>(filters);
                filterList.Add(new InputFilterLengthFilter(Element.MaxCharacters));
                Control.SetFilters(filterList.ToArray());
            }
            else
            {
                Control.SetFilters
                (
                    Control
                        .GetFilters()
                        .Where(x => !(x is InputFilterLengthFilter))
                        .ToArray()
                );
            }
        }

        public class CustomKeyboardEditText : EditText
        {
            readonly ExtendedEditor entry;

            public CustomKeyboardEditText(Context context, ExtendedEditor entry) : base(context)
            {
                this.entry = entry;
            }
        }
    }
}
