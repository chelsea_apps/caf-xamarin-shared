﻿﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Android.Content;
using Android.Graphics;
using Android.Text;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.Lang;
using ChelseaAppsFactory.Mobile.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Math = System.Math;

[assembly: ExportRenderer(typeof(FlatEntry), typeof(ChelseaAppsFactory.Mobile.Renderers.FlatEntryRenderer))]

namespace ChelseaAppsFactory.Mobile.Renderers
{
    public class FlatEntryRenderer : Xamarin.Forms.Platform.Android.AppCompat.ViewRenderer<FlatEntry, EditText>, ITextWatcher
    {
        bool disableTextUpdates;

        public FlatEntryRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<FlatEntry> e)
        {
            base.OnElementChanged(e);

            if(e.OldElement == null)
            {
                var ctrl = CreateNativeControl();
                SetNativeControl(ctrl);

                Control.InputType = InputTypes.ClassText;
                Control.AddTextChangedListener(this);

                SetText();
                SetHintText();
                SetBackgroundColor();
                SetTextColor();
                SetHintTextColor();
                SetKeyboard();
                SetFont();
                SetMaximumLength();
                Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
                Control.SetPadding(0, 0, 0, 0);
            }
        }

        void ITextWatcher.AfterTextChanged(IEditable s)
        {
        }

        void ITextWatcher.BeforeTextChanged(ICharSequence s, int start, int count, int after)
        {
        }

        void ITextWatcher.OnTextChanged(ICharSequence s, int start, int before, int count)
        {
            if(disableTextUpdates || (string.IsNullOrEmpty(Element.Text) && s.Length() == 0))
            {
                return;
            }

            if(Element.Formatter == null)
            {
                UpdateModelText(s.ToString());
            }
            else
            {
                var oldCursorPosition = Control.SelectionStart;
                var formattedText = Element.Formatter.Convert(Control.Text, null, null, null);

                if(Control.Text == formattedText) // Text hasn't changed, no need to update control
                {
                    UpdateModelTextFromFormatter(formattedText);
                    return;
                }

                disableTextUpdates = true;

                Control.SetText(formattedText, TextView.BufferType.Normal);
                var newText = s.ToString().Substring(start, count);

                if(!formattedText.Contains(newText))
                {
                    disableTextUpdates = false;
                    return;
                }

                var newPosition = oldCursorPosition;

                if(count == 1) // User typed one char
                {
                    // Check cursor is in correct position
                    var charPos = Math.Min(newPosition - 1, formattedText.Length - 1);
                    var rangeBeforeCursor = formattedText[charPos];

                    if(rangeBeforeCursor != s.ToArray()[start])
                    {
                        // Cursor is in the wrong position
                        newPosition += 1;
                    }
                }

                Control.SetSelection(ClampPosition(newPosition));

                UpdateModelTextFromFormatter(formattedText);

                disableTextUpdates = false;
            }
        }

        void UpdateModelTextFromFormatter(string text)
        {
            var modelText = Element.Formatter.ConvertBack(text, null, null, null);

            if(modelText != Element.Text)
            {
                UpdateModelText(modelText);
            }
        }

        void UpdateModelText(string value)
        {
            ((IElementController)Element).SetValueFromRenderer(Entry.TextProperty, value);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if(e.PropertyName == Entry.PlaceholderProperty.PropertyName)
            {
                SetHintText();
            }
            else if(e.PropertyName == Entry.TextColorProperty.PropertyName)
            {
                SetTextColor();
            }
            else if(e.PropertyName == VisualElement.BackgroundColorProperty.PropertyName)
            {
                SetBackgroundColor();
            }
            else if(e.PropertyName == Entry.TextProperty.PropertyName)
            {
                SetText();
            }
            else if(e.PropertyName == Entry.PlaceholderColorProperty.PropertyName)
            {
                SetHintTextColor();
            }
            else if(e.PropertyName == Entry.FontAttributesProperty.PropertyName ||
                    e.PropertyName == Entry.FontSizeProperty.PropertyName ||
                    e.PropertyName == Entry.FontFamilyProperty.PropertyName)
            {
                // This does not actually support changing font attributes or font family
                // Too many internal things in FontExtensions
                SetFont();
            }
            else if(e.PropertyName == InputView.KeyboardProperty.PropertyName || e.PropertyName == Entry.IsPasswordProperty.PropertyName)
            {
                SetKeyboard();
            }
            else if(e.PropertyName == FlatEntry.MaximumLengthProperty.PropertyName)
            {
                SetMaximumLength();
            }
        }

        void SetText()
        {
            if(disableTextUpdates)
            {
                return;
            }

            var text = (Element.Formatter != null)
                            ? Element.Formatter.Convert(Element.Text, null, null, null)
                            : Element.Text;

            if(text != Control.Text)
            {
                Control.Text = Element.Text;

                if(Control.IsFocused)
                {
                    Control.SetSelection(Control.Text.Length);
                }
            }
        }

        void SetMaximumLength()
        {
            if(Element.MaximumLength > -1)
            {
                var filters = Control.GetFilters();
                var filterList = filters == null ? new List<IInputFilter>() : new List<IInputFilter>(filters);
                filterList.Add(new InputFilterLengthFilter(Element.MaximumLength));
                Control.SetFilters(filterList.ToArray());
            }
            else
            {
                Control.SetFilters
                (
                    Control
                        .GetFilters()
                        .Where(x => !(x is InputFilterLengthFilter))
                        .ToArray()
                );
            }
        }

        public void SetBackgroundColor()
        {
            Control.SetBackgroundColor(Element.BackgroundColor.ToAndroid());
        }

        void SetHintText()
        {
            Control.Hint = Element.Placeholder;
        }

        void SetTextColor()
        {
            if(Element.TextColor == Xamarin.Forms.Color.Default)
            {
                Control.SetTextColor(Control.TextColors);
            }
            else
            {
                Control.SetTextColor(Element.TextColor.ToAndroid());
            }
        }

        void SetHintTextColor()
        {
            Control.SetHintTextColor(Element.PlaceholderColor.ToAndroid());
        }

        void SetFont()
        {
            Control.SetTextSize(ComplexUnitType.Sp, (float)Element.FontSize);
        }

        public override bool OnKeyPreIme(Android.Views.Keycode keyCode, Android.Views.KeyEvent e)
        {
            return base.OnKeyPreIme(keyCode, e);
        }

        void SetKeyboard()
        {
            if(Element.IsPassword)
            {
                Control.InputType = (Element.Keyboard == Keyboard.Numeric)
                    ? InputTypes.ClassNumber | InputTypes.NumberVariationPassword
                    : InputTypes.TextVariationPassword;
            }

            if(Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop)
            {
                Control.ShowSoftInputOnFocus = false;
            }
            else
            {
                Control.SetRawInputType(InputTypes.Null);
            }

            // Override Android defaulting password fields to sans-serif font
            UpdateFont();
        }

        int ClampPosition(int position)
        {
            return Math.Min(Math.Max(0, position), Control.Text.Length);
        }

        void UpdateFont()
        {
            Control.SetTypeface(Typeface.Create("sans-serif", TypefaceStyle.Normal), TypefaceStyle.Normal);
        }
    }
}