﻿using System;
using System.Threading.Tasks;

namespace ChelseaAppsFactory.Mobile.Routing
{
    public interface ITransitionOut
    {
        Task<bool> TransitionOut(object target);
        void Reset();
    }
}

