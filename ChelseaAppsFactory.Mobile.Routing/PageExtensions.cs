﻿using System;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Routing
{
	public static class PageExtensions
	{
		public static bool IsForViewModelType(this Page page, Type viewType)
		{
			if (page.BindingContext?.GetType() == viewType)
			{
				return true;
			}
			if (page is NavigationPage)
			{
				return ((NavigationPage)page).Navigation.NavigationStack[0].BindingContext?.GetType() == viewType;
			}
			return false;
		}
	}
}

