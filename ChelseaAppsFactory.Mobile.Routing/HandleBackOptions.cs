using System;
namespace ChelseaAppsFactory.Mobile.Routing
{
    public enum HandleBackOptions
    {
        PopModal,
        PopToRootModal,
        PopSinglePage,
        PopToRootSinglePage
    }
}
