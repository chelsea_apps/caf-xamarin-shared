﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using ChelseaAppsFactory.Mobile.Common;

namespace ChelseaAppsFactory.Mobile.Routing
{
    public static class HistoryHandler
    {
        public static void Handle(HistoryAction action, INavigation navigation, IReadOnlyList<Page> stack)
        {
            if(action is HistoryAction.ClearAllHistoryAction)
            {
                ClearAll(navigation, stack);
                return;
            }

            var clearToPage = action as HistoryAction.ClearToPageHistoryAction;
            if(clearToPage != null)
            {
                ClearToPage(navigation, stack, clearToPage.ClearToType);
                return;
            }
        }

        static void ClearAll(INavigation navigation, IReadOnlyList<Page> stack)
        {
            var pagesToRemove = stack.Reverse().Skip(1);

            foreach(var page in pagesToRemove)
            {
                navigation.RemovePage(page);
            }
        }

        static void ClearToPage(INavigation navigation, IReadOnlyList<Page> stack, Type pageType)
        {
            if(!stack.Any(p => p.GetType() != pageType))
            {
                Logger.Warn($"Tried to clear navigation stack to {pageType}, but it isn't in the stack");
                return;
            }

            foreach(var page in stack.Reverse().Skip(1).TakeWhile(p => p.GetType() != pageType))
            {
                navigation.RemovePage(page);
            }
        }
    }
    
}
