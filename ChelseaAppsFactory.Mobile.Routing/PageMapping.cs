﻿using System;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Routing
{
    public sealed class PageMapping
    {
        public PageMapping(Type viewType = null, Func<Page> pageFactory = null)
        {
            ViewType = viewType;
            PageFactory = pageFactory;
        }

        public Type ViewType { get; set; }

        public Func<Page> PageFactory { get; set; }
    }
}
