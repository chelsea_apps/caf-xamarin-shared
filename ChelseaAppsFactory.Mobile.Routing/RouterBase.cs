﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChelseaAppsFactory.Mobile.Common;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Routing
{
    public abstract class RouterBase
    {
        delegate Task RouteHandler(Type viewType, OpenViewMessage message);

        Dictionary<Type, RouteHandler> _handlers = new Dictionary<Type, RouteHandler>();
        Queue<OpenViewMessage> _viewQueue = new Queue<OpenViewMessage>();

        protected RouterBase(RouteConfig config)
        {
            foreach(var route in config.ModalPageRoutes)
            {
                _handlers.Add(route.Mapping.ViewType, (vt, msg) => HandleRoute(route, msg));
            }

            foreach(var route in config.SinglePageRoutes)
            {
                _handlers.Add(route.Mapping.ViewType, (vt, msg) => HandleRoute(route, msg));
            }

            foreach(var route in config.MultiPageRoutes)
            {
                foreach(var pm in route.Mappings)
                {
                    _handlers.Add(pm.ViewType, (vt, msg) => HandleRoute(route, vt, msg));
                }
            }
        }

        public abstract Page VisiblePage { get; }
        public NavigationPage RootPage { get; protected set; }

        [Obsolete("HandleBack(bool) is deprecated, please use HandleBack(HandleBackOptions) instead.")]
        protected abstract Task HandleBack(bool toRoot);
        protected abstract Task HandleBack(HandleBackOptions handleBackOption = HandleBackOptions.PopSinglePage);
        protected abstract Task HandleRoute(ModalPageRoute route, OpenViewMessage message);
        protected abstract Task HandleRoute(SinglePageRoute route, OpenViewMessage message);
        protected abstract Task HandleRoute(MultiPageRoute route, Type invokedType, OpenViewMessage message);

        public void Goto(OpenViewMessage message)
        {
            Logger.Info($"Goto {message.ViewType}");

            if(!message.CanQueue && _viewQueue.Any())
            {
                Logger.Info($"Goto in progress, discarding {message.ViewType.Name} due to disallowed queueing");
                return;
            }
            if(_viewQueue.Any(x => x.ViewType == message.ViewType))
            {
                Logger.Info($"Goto in progress, discarding {message.ViewType.Name} due to duplicate...");
                return;
            }
            _viewQueue.Enqueue(message);
#pragma warning disable RECS0165 // Asynchronous methods should return a Task instead of void
            Device.BeginInvokeOnMainThread(async () =>
            {
                await ProcessQueue();
            });
#pragma warning restore RECS0165 // Asynchronous methods should return a Task instead of void

        }

        readonly AsyncLock _locker = new AsyncLock();

        async Task ProcessQueue()
        {
            using(await _locker.LockAsync())
            {
                while(_viewQueue.Any())
                {
                    var message = _viewQueue.Peek();
                    Logger.Info($"Goto {message.ViewType.Name} processing...");

                    if(!_handlers.ContainsKey(message.ViewType))
                    {
                        throw new InvalidOperationException($"No route configured for {message.ViewType.Name}");
                    }

                    try
                    {
                        await _handlers[message.ViewType].Invoke(message.ViewType, message);
                    }
                    catch(Exception ex)
                    {
                        Logger.Crit($"Failed to handle OpenViewMessage for {message.ViewType}");
                        Logger.Exception(ex);
                        throw;
                    }
                    finally
                    {
                        _viewQueue.Dequeue();
                    }
                }
            }
        }

        [Obsolete("PopAsync(bool) is deprecated, please use PopAsync(HandleBackOptions) instead.")]
        public async Task PopAsync(bool toRoot)
        {
            using(await _locker.LockAsync())
            {
                await HandleBack(toRoot);
            }
        }

        public async Task PopAsync(HandleBackOptions handleBackOption = HandleBackOptions.PopSinglePage)
        {
            using(await _locker.LockAsync())
            {
                await HandleBack(handleBackOption);
            }
        }
    }
}
