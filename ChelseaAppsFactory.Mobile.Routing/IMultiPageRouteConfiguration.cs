﻿using System;
using ChelseaAppsFactory.Mobile.Common;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Routing
{
    public interface IMultiPageRouteConfiguration
    {
        IMultiPageRouteConfiguration ClearHistory(bool value = true);

        IMultiPageRouteConfiguration With<TModel, TPage>()
            where TModel : IPageViewModel
            where TPage : Page, new();

        IMultiPageRouteConfiguration With<TModel>(Func<Page> pageFactory)
            where TModel : IPageViewModel;
    }
}
