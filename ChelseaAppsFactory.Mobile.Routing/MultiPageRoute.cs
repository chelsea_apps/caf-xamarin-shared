﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChelseaAppsFactory.Mobile.Routing
{
    public class MultiPageRoute : RouteBase
    {
        public IList<PageMapping> Mappings { get; } = new List<PageMapping>();
    }
}
