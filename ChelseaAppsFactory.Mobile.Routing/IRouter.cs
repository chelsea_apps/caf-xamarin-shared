using System;
using System.Threading.Tasks;
using ChelseaAppsFactory.Mobile.Routing;

namespace ChelseaAppsFactory.Mobile.Common
{
    public interface IRouter
    {
        void Goto(OpenViewMessage message);
        [System.Obsolete("HandleBack(bool) is deprecated, please use HandleBack(HandleBackOptions) instead.")]
        Task PopAsync(bool toRoot);
        Task PopAsync(HandleBackOptions handleBackOption = HandleBackOptions.PopSinglePage);
    }
}
