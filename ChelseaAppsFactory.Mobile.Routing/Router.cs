using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChelseaAppsFactory.Mobile.Common;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Routing
{
    public class Router : RouterBase, IRouter
    {
		readonly Func<Type, IPageViewModel> _viewModelProvider;
        readonly Func<MultiPage<Page>> _multiPageProvider;

        public Router(RouteConfig config, NavigationPage rootPage, Func<Type, IPageViewModel> viewModelProvider, Func<MultiPage<Page>> multiPageProvider = null)
            : base(config)
        {
            _viewModelProvider = viewModelProvider;
            _multiPageProvider = multiPageProvider;

            RootPage = rootPage;
        }

        public void UpdateRootPage(NavigationPage rootPage)
        {
            RootPage = rootPage;
        }

        public override Page VisiblePage
        {
            get { return FindCurrentModal() ?? FindCurrentPage(); }
        }

        [Obsolete("HandleBack(bool) is deprecated, please use HandleBack(HandleBackOptions) instead.")]
        protected override async Task HandleBack(bool toRoot)
        {
            if(toRoot)
            {
                await HandleBack(HandleBackOptions.PopToRootSinglePage);
            }
            else
            {
                await HandleBack(HandleBackOptions.PopSinglePage);
            }
        }

        protected override async Task HandleBack(HandleBackOptions handleBackOption = HandleBackOptions.PopSinglePage)
        {
            var modal = FindCurrentModal();
            var page = FindCurrentPage();

            if(modal != null)
            {
                if(handleBackOption == HandleBackOptions.PopModal)
                {
                    if(modal.Navigation.NavigationStack.Count == 1)
                    {
                        await DismissModal();
                    }
                    else
                    {
                        await modal.Navigation.PopAsync();
                    }
                }
                else if(handleBackOption == HandleBackOptions.PopToRootModal)
                {
                    await DismissModal();
                }
            }
            else if(page != null)
            {
                if(handleBackOption == HandleBackOptions.PopToRootSinglePage)
                {
                    await page.Navigation.PopToRootAsync();
                }
                else if(handleBackOption == HandleBackOptions.PopSinglePage)
                {
                    await page.Navigation.PopAsync();
                }
            }
        }

        protected override async Task HandleRoute(ModalPageRoute route, OpenViewMessage message)
        {
            var page = CreatePage(route.Mapping, message.ViewInitializer);
            await OpenModal(page);
        }

        protected override async Task HandleRoute(SinglePageRoute route, OpenViewMessage message)
        {
            var page = CreatePage(route.Mapping, message.ViewInitializer);
            var containerPage = route.BrintToFront ? RootPage : FindContextualNavigationPage();

            await OpenPage(containerPage, page, message.HistoryAction ?? route.HistoryAction);
        }

        protected override async Task HandleRoute(MultiPageRoute route, Type invokedType, OpenViewMessage message)
        {
            if(_multiPageProvider == null)
            {
                throw new InvalidOperationException("Must specify a multipagehandler to use multipage");
            }
            await DismissModal();

            if(IsPageContainerFor(RootPage.CurrentPage, invokedType))
            {
                // If top level current page is a multipage and the container for the invoked type, select the appropriate page (tab)
                // to show the invoked types page.
                var containerPage = (MultiPage<Page>)RootPage.CurrentPage;
                containerPage.CurrentPage = containerPage.Children.FirstOrDefault(x => x.IsForViewModelType(invokedType));

                // If the page for the invoked type is a NavigationPage (almost always), pop the navigation to the root to ensure that
                // the view requested is actually visible (and not another page on the nested navigation)
                if(containerPage.CurrentPage is NavigationPage)
                {
                    await ((NavigationPage)containerPage.CurrentPage).PopToRootAsync();
                }
            }
            else
            {
                // If top level current page does not contain our page, we need to create a new MultiPage with all the pages (and view models)
                // created, initialize the requested invokedType with the specified initializer and select the corresponding page to be visible

                var pages = new List<NavigationPage>();
                foreach(var mapping in route.Mappings)
                {
                    var page = CreatePage(mapping, mapping.ViewType == invokedType ? message.ViewInitializer : _ => { });
                    var wrapper = CreateWrapperNavigationPage(page);
                    pages.Add(wrapper);
                }

                var multiPage = _multiPageProvider();
                NavigationPage.SetHasNavigationBar(multiPage, false);

                foreach(var p in pages)
                {
                    multiPage.Children.Add(p);
                }

                multiPage.CurrentPage = multiPage.Children.FirstOrDefault(x => x.IsForViewModelType(invokedType));
                await OpenPage(RootPage, multiPage, message.HistoryAction ?? route.HistoryAction);
            }
        }

        bool IsPageContainerFor(Page page, Type viewType)
        {
            var containerPage = page as MultiPage<Page>;
            if(containerPage != null)
            {
                return containerPage.Children.Any(x => x.IsForViewModelType(viewType));
            }

            return false;
        }

        Page CreatePage(PageMapping mapping, Action<IPageViewModel> viewInitializer)
        {
            var viewModel = _viewModelProvider(mapping.ViewType);
            viewInitializer(viewModel);

            var page = mapping.PageFactory();

            page.Appearing += (sender, e) => ((IPageViewModel)((Page)sender).BindingContext).OnAppearingAsync().LogIfFaulted();
            page.Disappearing += (sender, e) => ((IPageViewModel)((Page)sender).BindingContext).OnDisappearingAsync().LogIfFaulted();
            page.BindingContext = viewModel;

            viewModel.InitAsync().LogIfFaulted();

            return page;
        }

        NavigationPage CreateWrapperNavigationPage(Page page)
        {
            var navPage = new NavigationPage(page);
            navPage.SetBinding(Page.TitleProperty, new Binding(Page.TitleProperty.PropertyName, source: page));
            navPage.SetBinding(Page.IconProperty, new Binding(Page.IconProperty.PropertyName, source: page));
            navPage.SetBinding(NavigationPage.BarBackgroundColorProperty, new Binding("BarBackgroundColor", source: page));
            navPage.SetBinding(NavigationPage.BarTextColorProperty, new Binding("BarTextColor", source: page));

            return navPage;
        }

        async Task<bool> DismissModal()
        {
            if(RootPage.Navigation.ModalStack.Any())
            {
                await RootPage.Navigation.PopModalAsync();
                return true;
            }

            return false;
        }

        Page FindCurrentPage()
        {
            Page activePage = RootPage;

            while(true)
            {
                var containerPage = activePage as IPageContainer<Page>;
                if(containerPage == null)
                {
                    break;
                }

                activePage = containerPage.CurrentPage;
            }

            return activePage;
        }


        Page FindCurrentModal()
        {
            return (RootPage.Navigation.ModalStack.LastOrDefault() as NavigationPage)?.CurrentPage;
        }

        NavigationPage FindContextualNavigationPage()
        {
            var navigationPage = (RootPage.Navigation.ModalStack.LastOrDefault() as NavigationPage);
            if(navigationPage != null)
            {
                return navigationPage;
            }
            navigationPage = RootPage;
            Page activePage = RootPage;

            while(true)
            {
                var containerPage = activePage as IPageContainer<Page>;
                if(containerPage == null)
                {
                    break;
                }

                activePage = containerPage.CurrentPage;
                navigationPage = containerPage as NavigationPage ?? navigationPage;
            }

            return navigationPage;
        }

        async Task OpenModal(Page page)
        {
            var navPage = CreateWrapperNavigationPage(page);

            var currentPage = VisiblePage;
            if (VisiblePage?.GetType() == page?.GetType())
                return;
            
            var didTransition = await TransitionOut(currentPage, page);
            var animate = !didTransition;
            await RootPage.Navigation.PushModalAsync(navPage, animate);
            if(didTransition)
            {
                TransitionReset(currentPage);
            }
        }

        async Task OpenPage(NavigationPage containerPage, Page nextPage, HistoryAction historyAction)
        {
            var currentPage = FindCurrentPage();

            if(historyAction is HistoryAction.ClearAllHistoryAction)
            {
                nextPage.SetValue(NavigationPage.HasBackButtonProperty, false);
            }
            var didTransition = await TransitionOut(currentPage, nextPage);
            var animate = !didTransition;
            await containerPage.PushAsync(nextPage, animate);
            if(didTransition)
            {
                TransitionReset(currentPage);
            }

            var stack = containerPage.Navigation.NavigationStack;
            HistoryHandler.Handle(historyAction, RootPage.Navigation, stack);

            if(animate && Device.RuntimePlatform == Device.Android)
            {
                // Work around (hack) for Xamarin.Forms bug: PushAsync returns before the push has completed
                // Add an extra delay to ensure we don't return before the fragment animation has finished
                await Task.Delay(200);
            }
        }

        Task<bool> TransitionOut(Page currentPage, Page nextPage)
        {
            var toPage = currentPage as ITransitionOut;
            if(toPage != null)
            {
                return toPage.TransitionOut(nextPage);
            }

            return Task.FromResult(false);
        }

        void TransitionReset(Page page)
        {
            (page as ITransitionOut)?.Reset();
        }
    }
}
