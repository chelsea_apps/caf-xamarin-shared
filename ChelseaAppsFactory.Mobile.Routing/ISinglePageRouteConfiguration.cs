﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Routing
{
    public interface ISinglePageRouteConfiguration
    {
        ISinglePageRouteConfiguration BringToFront(bool value = true);

        ISinglePageRouteConfiguration ClearHistory(bool value = true);

        ISinglePageRouteConfiguration ClearHistoryTo<T>() where T : ContentPage;
    }
}
