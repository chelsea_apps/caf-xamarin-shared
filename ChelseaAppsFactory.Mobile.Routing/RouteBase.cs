﻿using ChelseaAppsFactory.Mobile.Common;

namespace ChelseaAppsFactory.Mobile.Routing
{
    public abstract class RouteBase
    {
        public HistoryAction HistoryAction { get; set; }
    }
}
