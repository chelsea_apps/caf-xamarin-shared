﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Routing
{
    public class RouteConfig
    {
        public RouteConfig(IReadOnlyList<ModalPageRoute> modalPageRoutes, IReadOnlyList<SinglePageRoute> singlePageRoutes, IReadOnlyList<MultiPageRoute> multiPageRoutes)
        {
            ModalPageRoutes = modalPageRoutes;
            SinglePageRoutes = singlePageRoutes;
            MultiPageRoutes = multiPageRoutes;
        }

        public IReadOnlyList<ModalPageRoute> ModalPageRoutes { get; }

        public IReadOnlyList<SinglePageRoute> SinglePageRoutes { get; }

        public IReadOnlyList<MultiPageRoute> MultiPageRoutes { get; }
    }
}
