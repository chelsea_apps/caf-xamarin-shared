﻿using System;
using System.Collections.Generic;
using ChelseaAppsFactory.Mobile.Common;
using Xamarin.Forms;

namespace ChelseaAppsFactory.Mobile.Routing
{
    public class RouteConfigBuilder
    {
        List<ModalPageRoute> _modalPageRoutes;
        List<SinglePageRoute> _singlePageRoutes;
        List<MultiPageRoute> _multiPageRoutes;

        public RouteConfigBuilder()
        {
            Build();
        }

        public RouteConfig Build()
        {
            try
            {
                return new RouteConfig(_modalPageRoutes, _singlePageRoutes, _multiPageRoutes);
            }
            finally
            {
                _modalPageRoutes = new List<ModalPageRoute>();
                _singlePageRoutes = new List<SinglePageRoute>();
                _multiPageRoutes = new List<MultiPageRoute>();
            }
        }

        public void MapModal<TModel, TPage>()
            where TModel : IPageViewModel
            where TPage : Page, new()
        {
            MapModal<TModel>(() => new TPage());
        }

        public void MapModal<TModel>(Func<Page> pageFactory)
            where TModel : IPageViewModel
        {
            var route = new ModalPageRoute
            {
                Mapping = new PageMapping(typeof(TModel), pageFactory)
            };

            _modalPageRoutes.Add(route);
        }

        public ISinglePageRouteConfiguration MapSinglePage<TModel, TPage>()
            where TModel : IPageViewModel
            where TPage : Page, new()
        {
            return MapSinglePage<TModel>(() => new TPage());
        }

        public ISinglePageRouteConfiguration MapSinglePage<TModel>(Func<Page> pageFactory)
            where TModel : IPageViewModel
        {
            var route = new SinglePageRoute
            {
                Mapping = new PageMapping(typeof(TModel), pageFactory)
            };

            _singlePageRoutes.Add(route);

            return new SinglePageRouteConfigurationImpl(route);
        }

        public IMultiPageRouteConfiguration MapMultiPage()
        {
            var route = new MultiPageRoute();

            _multiPageRoutes.Add(route);

            return new MultiPageRouteConfigurationImpl(route);
        }

        class SinglePageRouteConfigurationImpl : ISinglePageRouteConfiguration
        {
            readonly SinglePageRoute _route;

            public SinglePageRouteConfigurationImpl(SinglePageRoute route)
            {
                _route = route;
            }

            public ISinglePageRouteConfiguration BringToFront(bool value = true)
            {
                _route.BrintToFront = value;
                return this;
            }

            public ISinglePageRouteConfiguration ClearHistory(bool value = true)
            {
                _route.HistoryAction = HistoryAction.ClearAll;
                return this;
            }
            
            public ISinglePageRouteConfiguration ClearHistoryTo<T>() where T : ContentPage
            {
                _route.HistoryAction = HistoryAction.ToPage(typeof(T));
                return this;
            }
        }

        class MultiPageRouteConfigurationImpl : IMultiPageRouteConfiguration
        {
            MultiPageRoute _route;

            public MultiPageRouteConfigurationImpl(MultiPageRoute route)
            {
                _route = route;
            }

            public IMultiPageRouteConfiguration ClearHistory(bool value = true)
            {
                _route.HistoryAction = HistoryAction.ClearAll;
                return this;
            }

            public IMultiPageRouteConfiguration With<TModel>(Func<Page> pageFactory) where TModel : IPageViewModel
            {
                _route.Mappings.Add(new PageMapping(typeof(TModel), pageFactory));
                return this;
            }

            public IMultiPageRouteConfiguration With<TModel, TPage>()
                where TModel : IPageViewModel
                where TPage : Page, new()
            {
                return With<TModel>(() => new TPage());
            }
        }
    }
}
